package com.atmosquare.commons.core.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class ListingOptions {

    public static final String DEFAULT_COLLECTION_SEPARATOR = ",";
    public static final String DEFAULT_SORTING_PARAMETER = "sorting";

    public static ListingOptions fromMap(Map<String, List<String>> parameters, String collectionSeparator, String sortingParameterName, List<String> ignoredParameters) {
        List<ListingFilter> filters = new ArrayList<ListingFilter>();

        if (collectionSeparator == null) {
            collectionSeparator = DEFAULT_COLLECTION_SEPARATOR;
        }

        if (sortingParameterName == null) {
            sortingParameterName = DEFAULT_SORTING_PARAMETER;
        }

        for (String par : parameters.keySet()) {
            if (sortingParameterName.equals(par)) {
                continue;
            }

            if(ignoredParameters.contains(par)){
                continue;
            }

            String parName = par;
            FilterOperator op = FilterOperator.Eq;
            List<String> values = parameters.get(par);

            for (FilterOperator oper : FilterOperator.values()) {
                String suffix = "(" + oper.name().toLowerCase() + ")";

                if (par.endsWith(suffix)) {
                    op = oper;
                    parName = parName.replace(suffix, "");
                    break;
                }
            }

            ListingFilter filter = new ListingFilter();
            filter.Name = parName;
            filter.Operator = op;
            filter.Values = Arrays.asList(values.get(0).split(collectionSeparator));

            filters.add(filter);
        }

        List<ListingSort> sortingList = new ArrayList<>();

        if (parameters.containsKey(sortingParameterName)) {
            String sortingStr = String.join(collectionSeparator, parameters.get(sortingParameterName));

            List<String> sortingParts = Arrays.asList(sortingStr.split(collectionSeparator));
            for (String sort : sortingParts) {
                if (sort.startsWith("-")) {
                    ListingSort fieldSort = new ListingSort();
                    fieldSort.Direction = SortDirecton.Desc;
                    fieldSort.Name = sort.substring(1);
                    sortingList.add(fieldSort);
                } else {
                    ListingSort fieldSort = new ListingSort();
                    fieldSort.Direction = SortDirecton.Asc;
                    fieldSort.Name = sort.replace("+", "");
                    sortingList.add(fieldSort);
                }
            }
        }

        ListingOptions options = new ListingOptions();
        options.filters = filters;
        options.sorts = sortingList;
        return options;
    }

    private List<ListingFilter> filters;
    private List<ListingSort> sorts;

    public List<ListingFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<ListingFilter> filters) {
        this.filters = filters;
    }

    public List<ListingSort> getSorts() {
        return sorts;
    }

    public void setSorts(List<ListingSort> sorts) {
        this.sorts = sorts;
    }

    public enum FilterOperator {
        N,
        Nn,
        Eq,
        Neq,
        In,
        Nin,
        Lt,
        Lte,
        Gt,
        Gte,
        Cw,
        Ncw,
        Sw,
        Nsw,
        Fw,
        Nfw
    }

    public enum SortDirecton {
        Asc,
        Desc
    }

    public static class ListingFilter {
        public String Name;
        public FilterOperator Operator;
        public List<String> Values;
    }

    public static class ListingSort {
        public String Name;
        public SortDirecton Direction;
    }
}

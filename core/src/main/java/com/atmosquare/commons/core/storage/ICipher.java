package com.atmosquare.commons.core.storage;

public interface ICipher {

    String encrypt(String strValue);
    String decrypt(String encryptedValue);

}
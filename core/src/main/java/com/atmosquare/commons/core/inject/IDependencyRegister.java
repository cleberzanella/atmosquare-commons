package com.atmosquare.commons.core.inject;

public interface IDependencyRegister {
    default <T> void register(Class<T> instanceClass, Class<T> concreteClass){
        registerUntyped(instanceClass, concreteClass);
    }
    void registerUntyped(Class<?> instanceClass, Class<?> concreteClass);
}

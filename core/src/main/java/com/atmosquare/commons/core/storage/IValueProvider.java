package com.atmosquare.commons.core.storage;

/**
 * Quando é necessário passar um parâmetro cujo valor pode ser alterado a medida que o programa executa. Este
 * provider sempre retornará o valor atualizado.
 *
 * Por exemplo: O Agente NFCe ao ser inicializado precisa que seja informado a URL do Host da retaguarda, e esta
 * configuração pode ser alterada a medida que o agente executa.
 *
 * @param <T>
 */
public interface IValueProvider<T> {

    T get();
    T getOrElse(T defaultValue);

    abstract class AbstractValueProvider<T> implements IValueProvider<T> {

        public abstract T get();

        public T getOrElse(T defaultValue) {
            T value = get();

            if(value == null){
                return defaultValue;
            }

            return value;
        }
    }

    class ConstantValue<T> extends AbstractValueProvider<T> {

        private final T constValue;

        public ConstantValue(T constValue) {
            this.constValue = constValue;
        }


        @Override
        public T get() {
            return constValue;
        }

    }

}
package com.atmosquare.commons.core.inject;

import java.io.Closeable;

public interface IDependencyInjector extends Closeable {
    IDependencyInjector openScope();
    <T> T resolve(Class<T> clazz);
}

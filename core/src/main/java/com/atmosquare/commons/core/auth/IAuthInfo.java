package com.atmosquare.commons.core.auth;

public interface IAuthInfo {
    void setAuthId(Long authId);
    Long getAuthId();
    void setTenantId(Long tenantId);
    Long getTenantId();
}

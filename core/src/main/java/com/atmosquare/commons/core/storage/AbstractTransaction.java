package com.atmosquare.commons.core.storage;

public abstract class AbstractTransaction implements ITransaction {

    private boolean active;
    private boolean committed;
    private boolean rolledBack;
    private TransactionChange onChangeEvent;

    @Override
    public boolean isActive() {
        return active;
    }

    protected void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean wasCommitted() {
        return committed;
    }

    protected void setCommitted(boolean committed) {
        this.committed = committed;
    }

    @Override
    public boolean wasRolledBack() {
        return rolledBack;
    }

    protected void setRolledBack(boolean rolledBack) {
        this.rolledBack = rolledBack;
    }

    @Override
    public void setOnChange(TransactionChange changeEvent) {
        this.onChangeEvent = changeEvent;
    }

    protected TransactionChange getOnChangeEvent() {
        return onChangeEvent;
    }
}
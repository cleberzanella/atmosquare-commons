package com.atmosquare.commons.core.storage;

public interface ITransaction extends AutoCloseable {

    boolean isActive();
    boolean wasCommitted();
    boolean wasRolledBack();
    void setOnChange(TransactionChange changeEvent);

    void commit();
    void rollback();

    @Override
    void close();

    interface TransactionChange {
        void onChange(ITransaction transaction);
    }

}
package com.atmosquare.commons.core.auth;

public interface IAuthInfoProvider {
    IAuthInfo getAuthInfo();
    void setAuthInfo(IAuthInfo authInfo);
}

package com.atmosquare.commons.core.storage;

public interface IKeyValueStorage {

    ITransaction openTransaction();

    void putString(String key, String value);
    void putInt(String key, Integer value);
    void putBoolean(String key, Boolean value);

    void putEncryptedString(String key, String value);
    void putEncryptedInt(String key, Integer value);

    String getString(String key);
    Integer getInt(String key);
    Integer getIntOrElse(String key, Integer defaultValue);
    Boolean getBoolean(String key);
    Boolean getBooleanOrElse(String key, Boolean defaultValue);

    String getEncryptedString(String key);
    Integer getEncryptedInt(String key);
    Integer getEncryptedIntOrElse(String key, Integer defaultValue);

    class StringValueProvider  extends IValueProvider.AbstractValueProvider<String>{

        private final String key;
        private final IKeyValueStorage keyValueStorage;

        public StringValueProvider(String key, IKeyValueStorage keyValueStorage) {
            this.key = key;
            this.keyValueStorage = keyValueStorage;
        }

        @Override
        public String get() {
            try(ITransaction transaction = keyValueStorage.openTransaction()){
                return keyValueStorage.getString(key);
            }
        }
    }

    class IntegerValueProvider extends IValueProvider.AbstractValueProvider<Integer>{

        private final String key;
        private final IKeyValueStorage keyValueStorage;

        public IntegerValueProvider(String key, IKeyValueStorage keyValueStorage) {
            this.key = key;
            this.keyValueStorage = keyValueStorage;
        }

        @Override
        public Integer get() {
            try(ITransaction transaction = keyValueStorage.openTransaction()){
                return keyValueStorage.getInt(key);
            }
        }
    }

    class LongValueProvider extends IValueProvider.AbstractValueProvider<Long>{

        private final String key;
        private final IKeyValueStorage keyValueStorage;

        public LongValueProvider(String key, IKeyValueStorage keyValueStorage) {
            this.key = key;
            this.keyValueStorage = keyValueStorage;
        }

        @Override
        public Long get() {
            try(ITransaction transaction = keyValueStorage.openTransaction()){
                Number intValue = keyValueStorage.getInt(key);
                return intValue == null ? null : intValue.longValue();
            }
        }
    }

}
package com.atmosquare.commons.core.auth;

import java.util.Locale;

public interface ILocaleInfo {
    Locale getPreferredLocale();
    void setPreferredLocale(Locale locale);
}

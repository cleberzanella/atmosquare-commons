package com.atmosquare.commons.externalref.model;

public class ExternalReferenceTag {

    private String group;
    private String value;
    private Boolean restricted; // não listado por padrão

    public ExternalReferenceTag(){

    }

    public ExternalReferenceTag(String group, String value) {
        this.group = group;
        this.value = value;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Boolean getRestricted() {
        return restricted;
    }

    public void setRestricted(Boolean restricted) {
        this.restricted = restricted;
    }
}

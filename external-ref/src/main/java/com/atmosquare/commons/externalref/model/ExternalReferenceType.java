package com.atmosquare.commons.externalref.model;

public class ExternalReferenceType {

    public enum Direction {
        IN,
        OUT
    }

    private int moduleNumber;
    private String entityName;
    private Direction direction;

    public int getModuleNumber() {
        return moduleNumber;
    }

    public void setModuleNumber(int moduleNumber) {
        this.moduleNumber = moduleNumber;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}

package com.atmosquare.commons.externalref.model;

public class ExternalReference {

    private String uid;
    private DateAudit dateAudit;
    private String shortDescription;
    private String longDescription;
    private ExternalReferenceTag[] tags;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public DateAudit getDateAudit() {
        return dateAudit;
    }

    public void setDateAudit(DateAudit dateAudit) {
        this.dateAudit = dateAudit;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public ExternalReferenceTag[] getTags() {
        return tags;
    }

    public void setTags(ExternalReferenceTag[] tags) {
        this.tags = tags;
    }
}

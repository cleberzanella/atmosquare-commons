package com.atmosquare.commons.externalref.model;

import java.time.Instant;

public class DateAudit {

    private Instant createdAt;
    private Instant updatedAt;
    private Instant removeddAt;

    public DateAudit(){

    }

    public DateAudit(Instant createdAt, Instant updatedAt, Instant removeddAt) {
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.removeddAt = removeddAt;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Instant getRemoveddAt() {
        return removeddAt;
    }

    public void setRemoveddAt(Instant removeddAt) {
        this.removeddAt = removeddAt;
    }
}

package com.atmosquare.commons.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class JsonUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

    public static Map<String, Object> parseMap(String jsonString){
        try {
            return new ObjectMapper().readValue(jsonString, HashMap.class);
        } catch (JsonProcessingException e) {
            LOGGER.error("Erro ao converter JSON para mapa: " + jsonString, e);
        }
        return null;
    }

    public static <T> T parseObject(String jsonString, Class<T> objClass){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());

        try {
            return mapper.readValue(jsonString, objClass);
        } catch (JsonProcessingException e) {
            LOGGER.error("Erro ao converter JSON para objeto: " + jsonString, e);
        }
        return null;
    }

    public static <T, P> T parseGeneric(String jsonString, Class<T> genericClass , Class<P> contentClass){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.registerModule(new JavaTimeModule());
        JavaType type = mapper.getTypeFactory().constructParametricType(genericClass, contentClass);
        try {
            return mapper.readValue(jsonString, type);
        } catch (JsonProcessingException e) {
            LOGGER.error("Erro ao converter JSON para objeto genérico: " + jsonString, e);
        }
        return null;
    }

    public static String toJson(Object obj){
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.registerModule(new JavaTimeModule());
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            LOGGER.error("Erro ao converter objeto \"" + (obj == null ? "null" : obj.getClass()) + "\" para JSON.", e);
        }
        return null;
    }
}

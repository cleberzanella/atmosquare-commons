package com.atmosquare.commons.webrequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class WebRequestConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebRequestConfiguration.class);

    private Map<String,String> headers = new HashMap<>();
    private Long timeoutInSeconds;
    //private IValueProvider<ConfiguracaoProxy> proxyProvider;

    public WebRequestConfiguration(){

    }

    public WebRequestConfiguration(WebRequestConfiguration inner){
        for(Map.Entry<String,String> entry : inner.headers.entrySet()){
            headers.put(entry.getKey(), entry.getValue());
        }
    }

    public void applyConfiguration(HttpURLConnection urlConnection){
        if(timeoutInSeconds != null){
            urlConnection.setConnectTimeout((int) (timeoutInSeconds * 1000));
            urlConnection.setReadTimeout((int) (timeoutInSeconds * 1000));
        }

        for(Map.Entry<String,String> header : headers.entrySet()){
            urlConnection.setRequestProperty(header.getKey(),header.getValue());
        }
    }

    public HttpURLConnection openConnection(URL url) throws IOException {

        Proxy proxy = null;

        //final ConfiguracaoProxy configuracaoProxy = proxyProvider.get();
//            if(configuracaoProxy != null){
//                Authenticator authenticator = new Authenticator() {
//                    @Override
//                    protected PasswordAuthentication getPasswordAuthentication() {
//                        return new PasswordAuthentication(configuracaoProxy.getUser(), configuracaoProxy.getPassword().toCharArray());
//                    }
//                };
//
//                URL proxyUrl;
//                try {
//                    proxyUrl = new URL(configuracaoProxy.getProxyServer());
//
//                    Authenticator.setDefault(authenticator);
//
//                    proxy = new Proxy(Proxy.Type.HTTP,
//                            new InetSocketAddress(proxyUrl.getHost(), proxyUrl.getPort()));
//
//                } catch (MalformedURLException e) {
//                    LOGGER.error(String.format("Erro ao fazer parse da URL do  Proxy de rede: %s", configuracaoProxy.getProxyServer()), e);
//                }
//
//            }

        if(proxy != null){
            return (HttpURLConnection) url.openConnection(proxy);
        }

        return (HttpURLConnection) url.openConnection();
    }

    public void setTimeout(Long timeoutInSeconds){
        this.timeoutInSeconds = timeoutInSeconds;
    }

    public void setHeader(String headerName, String headerValue) {
        if(headerValue == null){
            headers.remove(headerName);
        } else {
            headers.put(headerName, headerValue);
        }
    }

}

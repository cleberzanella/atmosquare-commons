package com.atmosquare.commons.webrequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLHandshakeException;
import java.io.*;
import java.net.*;

public class WebRequestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebRequestConfiguration.class);
    private static final int BUFFER_SIZE = 4096;

    public static WebResult request(String urlStr, String method, String bodyData, boolean hasResponse, WebRequestConfiguration configuration) {

        HttpURLConnection con = null;
        try {
            URL url = new URL(urlStr);
            con = configuration.openConnection(url);

            //if(!method.equals("GET")){
                con.setRequestMethod(method);

                con.setDoOutput(hasResponse);
                con.setDoInput(bodyData != null);
            //}

            if(configuration != null){
                configuration.applyConfiguration(con);
            }

            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream()); // freezes on poor connection

            if (bodyData != null) {
                wr.write(bodyData);
            }
            wr.flush();
            wr.close();

            int status = con.getResponseCode();

            if (!hasResponse) {
                return new WebResult(status, null);
            }

            InputStream inputStream;

            if (status >= HttpURLConnection.HTTP_BAD_REQUEST) {
                inputStream = con.getErrorStream();
            } else {
                inputStream = con.getInputStream();
            }

            StringBuilder result = new StringBuilder();
            if(inputStream != null){
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
            }

            String bodyResult = null;

            if (result.length() > 0) {
                bodyResult = result.toString();
            }

            return new WebResult(status, bodyResult);
        } catch (ProtocolException e) {
            throw new ConnectException(e);
        } catch (MalformedURLException e) {
            throw new ConnectException(e);
        } catch (ConnectException exc) {
            // could not contact server
            throw new ConnectException(exc);
        } catch (UnknownHostException e) {
            // airplane mode
            throw new ConnectException(e);
        } catch (SocketException exc) {
            // unstable network
            throw new ConnectException(exc);
        } catch(SSLHandshakeException exc){
            LOGGER.error("Falha ao validar certificado SSL. Solução: Normalmente o problema é associado a sub-sub domínios, é possível ignorar esta validação.");
            // unable to find valid certification path
            throw new ConnectException(exc);
        } catch (IOException e) {
            throw new ConnectException(e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }
    }

    public static WebResult getRequest(String urlStr){
        WebRequestConfiguration conf = new WebRequestConfiguration();
        conf.setHeader("Content-Type", "application/json");
        return getRequest(urlStr, conf);
    }

    public static WebResult getRequest(String urlStr, WebRequestConfiguration conf) {

        HttpURLConnection con = null;
        try {
            URL obj = new URL(urlStr);
            con = conf.openConnection(obj);

            // optional default is GET
            con.setRequestMethod("GET");

            if(conf != null){
                conf.applyConfiguration(con);
            }

            //add request header
            //con.setRequestProperty("User-Agent", DEFAULT_USER_AGENT_HEADER);

            int responseCode = con.getResponseCode();

            String response = getTextResult(con, responseCode);

            String xmlResponse = null;

            if (response.length() > 0) {
                xmlResponse = response;
            }

            WebResult retorno = new WebResult(responseCode, xmlResponse);
            return retorno;

        } catch (ProtocolException e) {
            throw new ConnectException(e);
        } catch (MalformedURLException e) {
            throw new ConnectException(e);
        } catch (ConnectException exc) {
            // could not contact server
            throw new ConnectException(exc);
        } catch (UnknownHostException e) {
            // airplane mode
            throw new ConnectException(e);
        } catch (SocketException exc) {
            // unstable network
            throw new ConnectException(exc);
        } catch(SSLHandshakeException exc){
            LOGGER.error("Falha ao validar certificado SSL. Solução: Normalmente o problema é associado a sub-sub domínios.");
            // unable to find valid certification path
            throw new ConnectException(exc);
        } catch (IOException e) {
            throw new ConnectException(e);
        } finally {
            if (con != null) {
                con.disconnect();
            }
        }

    }

    public static WebResultFile downloadFile(String urlStr, WebRequestConfiguration conf, String saveDir){
        return downloadFile(urlStr, conf, saveDir, true);
    }
    public static WebResultFile downloadFile(String urlStr, WebRequestConfiguration conf, String saveDir, boolean reportProgess) {

        HttpURLConnection httpConn = null;
        try {
            URL url = new URL(urlStr);
            httpConn = conf.openConnection(url);

            // optional default is GET
            httpConn.setRequestMethod("GET");

            if(conf != null){
                conf.applyConfiguration(httpConn);
            }

            int responseCode = httpConn.getResponseCode();

            // always check HTTP response code first
            if (responseCode != HttpURLConnection.HTTP_OK) {

                String responseText = getTextResult(httpConn, responseCode);
                LOGGER.debug("No file to download. Server replied HTTP code: " + responseCode + " " + responseText);
                return new WebResultFile(responseCode, null);
            }

            String fileName = "";
            String disposition = httpConn.getHeaderField("Content-Disposition");
            String contentType = httpConn.getContentType();
            int contentLength = httpConn.getContentLength();

            if (disposition != null) {
                // extracts file name from header field
                final String fileNameToken = "filename=";
                int index = disposition.indexOf(fileNameToken);
                if (index > 0) {
                    int startIndex = index + fileNameToken.length() + 1;
                    int endIndex = disposition.indexOf('"', startIndex);
                    fileName = disposition.substring(startIndex, endIndex);
                }
            } else {
                // extracts file name from URL
                fileName = urlStr.substring(urlStr.lastIndexOf("/") + 1);

                if(fileName.contains("?")){
                    fileName = fileName.substring(0, fileName.indexOf("?"));
                }

            }

            if(reportProgess){
                LOGGER.debug("Content-Type = {} , Content-Length = {} , fileName = {}", contentType, contentLength, fileName);
            }

            // opens input stream from the HTTP connection
            InputStream inputStream = httpConn.getInputStream();

            if(saveDir != null){
                new File(saveDir).mkdirs();
            }

            // opens an output stream to save into file
            String filePath = saveDir == null || saveDir.trim().length() == 0 ? null : saveDir + File.separator + fileName;
            OutputStream outputStream = filePath == null ?  new ByteArrayOutputStream() : new FileOutputStream(filePath);

            int bytesRead;
            int totalBytesRead = 0;
            double lastPercent = 1;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
                totalBytesRead += bytesRead;
                if(contentLength > 0){
                    double remainingPercent = (contentLength - totalBytesRead) / (double) contentLength;
                    if(lastPercent - remainingPercent >= .01){
                        lastPercent = remainingPercent;
                        if(reportProgess){
                            LOGGER.info("Remaining file " + fileName + " " + (remainingPercent * 100) + "%");
                        }
                    }
                }

            }

            outputStream.close();
            inputStream.close();

            if(reportProgess){
                LOGGER.debug("File downloaded");
            }

            InputStream resultInputStream = outputStream instanceof ByteArrayOutputStream ? new ByteArrayInputStream(((ByteArrayOutputStream) outputStream).toByteArray()) : new FileInputStream(filePath);
            return new WebResultFile(responseCode, fileName, resultInputStream);
        } catch (IOException exc){
            throw new ConnectException(exc);
        } finally {
            if (httpConn != null) {
                httpConn.disconnect();
            }
        }
    }

    private static String getTextResult(HttpURLConnection httpConn, int responseCode) throws IOException {
        InputStream inputStream;

        if (responseCode >= HttpURLConnection.HTTP_BAD_REQUEST) {
            inputStream = httpConn.getErrorStream();
        } else {
            inputStream = httpConn.getInputStream();
        }

        StringBuilder response = new StringBuilder();
        if(inputStream != null){
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();
        }
        return response.toString();
    }

    public static class ConnectException extends RuntimeException {

        public ConnectException(Throwable cause) {
            super(cause);
        }

    }

    public static class WebResult {

        private int httpCode;
        private String responseBody;

        public WebResult(int httpCode, String responseBody) {
            this.httpCode = httpCode;
            this.responseBody = responseBody;
        }

        public int getHttpCode() {
            return httpCode;
        }

        public String getResponseBody() {
            return responseBody;
        }
    }

    public static class WebResultFile {

        private int httpCode;
        private String fileName;
        private InputStream inputStream;

        public WebResultFile(int httpCode, String fileName) {
            this.httpCode = httpCode;
            this.fileName = fileName;
        }

        public WebResultFile(int httpCode, String fileName, InputStream inputStream) {
            this.httpCode = httpCode;
            this.fileName = fileName;
            this.inputStream =inputStream;
        }

        public int getHttpCode() {
            return httpCode;
        }

        public String getFileName() {
            return fileName;
        }

        public InputStream getInputStream() {
            return inputStream;
        }
    }

}

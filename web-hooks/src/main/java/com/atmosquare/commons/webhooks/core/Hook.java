package com.atmosquare.commons.webhooks.core;

import java.util.Map;

public class Hook {
    public long hookId;
    public Map<String, String> templates;
    public Map<String, String> renderedTemplates;
    public boolean ignored;
    public String renderingErrors;

    public boolean templatesContains(String str) {
        for (Map.Entry<String, String> entry : templates.entrySet()) {
            if (entry.getValue() != null && entry.getValue().contains(str)) {
                return true;
            }
        }

        return false;
    }

    public long getHookId() {
        return hookId;
    }

    public void setHookId(long hookId) {
        this.hookId = hookId;
    }

    public Map<String, String> getTemplates() {
        return templates;
    }

    public void setTemplates(Map<String, String> templates) {
        this.templates = templates;
    }

    private String getTemplateOrElse(Map<String, String> templatesMap ,String templateName, String str) {
        if (templatesMap == null || !templatesMap.containsKey(templateName)) {
            return str;
        }

        return templatesMap.get(templateName);
    }

    public String getTemplateOrElse(String templateName, String str) {
        return getTemplateOrElse(templates, templateName, str);
    }

    public String getRenderedTemplateOrElse(String templateName, String str) {
        return getTemplateOrElse(renderedTemplates, templateName, str);
    }

    public Map<String, String> getRenderedTemplates() {
        return renderedTemplates;
    }

    public void setRenderedTemplates(Map<String, String> renderedTemplates) {
        this.renderedTemplates = renderedTemplates;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    public String getRenderingErrors() {
        return renderingErrors;
    }

    public void setRenderingErrors(String renderingErrors) {
        this.renderingErrors = renderingErrors;
    }
}

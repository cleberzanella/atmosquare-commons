package com.atmosquare.commons.webhooks.core;

public interface IHookContextLoaderFactory {
    AbstractHookContextLoader forHookType(Object hookType);
}

package com.atmosquare.commons.webhooks.core;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractHookContextLoader {

    protected ITemplateRenderer templateRenderer;

    public void setTemplateRenderer(ITemplateRenderer templateRenderer) {
        this.templateRenderer = templateRenderer;
    }

    public abstract boolean matchHook(Object hookType);

    public abstract List<Hook> getHooks();

    public abstract HookTargetApi loadTargetContext(Hook hook);

    public abstract Object loadContext(Object hookInput, Hook hook);

    public abstract void registerHookEventForDelivery(Hook hook, Map<String, Object> generalContext);

    protected void renderHookTemplates(Hook hook, Map<String, Object> templateContext, Object summary) {
        if (hook.renderedTemplates == null) {
            hook.renderedTemplates = new HashMap<>();
        }

        var sb = new StringBuilder();

        for (Map.Entry<String, String> entry : hook.getTemplates().entrySet()) {
            var subSummary = newSummary();

            String templateName = entry.getKey();
            String template = entry.getValue();

            if (getResponseConditionTemplateName().equals(templateName)) {
                // só deve ser avaliado após obter resposta do request
                hook.getRenderedTemplates().put(templateName, null);
                continue;
            }

            if(template == null)
                template = "";

            // TODO alguns campos tem valores predefinidos que pode ser validados, exemplo bodytype: raw, json, xml, urlencoded, formdata
            String renderResult = templateRenderer.renderTemplate(template, templateContext, subSummary);

            if (renderResult == null || renderResult.trim().isEmpty()) {
                renderResult = null;
            }

            hook.getRenderedTemplates().put(templateName, renderResult);

            sb.append(describeSummary(subSummary, templateName));
        }

        if (sb.length() > 0) {
            hook.renderingErrors = sb.toString();
        }
    }

    public void renderHooksAndRegisterForDelivery(Object hookInput, List<Hook> hooks) {
        Object input;
        if (hookInput instanceof String || hookInput instanceof Boolean || hookInput instanceof Integer || hookInput instanceof Long) {
            input = hookInput;
        } else {
            input = jsonToMap(objToJson(hookInput));
        }

        // busca o contexto só uma vez para todos os hooks
        var allTemplatesHook = new Hook();
        allTemplatesHook.setTemplates(new HashMap<>());
        for (var hook : hooks) {
            for (var entry : hook.getTemplates().entrySet()) {
                if (!allTemplatesHook.getTemplates().containsKey(entry.getKey())) {
                    allTemplatesHook.getTemplates().put(entry.getKey(), "");
                }

                String templStr = hook.getTemplates().get(entry.getKey());
                if (templStr != null && !templStr.isEmpty()) {
                    allTemplatesHook.getTemplates().put(entry.getKey(), allTemplatesHook.getTemplates().get(entry.getKey()) + hook.getTemplates().get(entry.getKey()));
                }

            }
        }

        var context = loadContext(hookInput, allTemplatesHook);
        var contextDict = context == null ? null : jsonToMap(objToJson(context));

        for (var hook : hooks) {
            hook.setIgnored(allTemplatesHook.isIgnored());

            var targetContext = loadTargetContext(hook);
            var targetContextDict = jsonToMap(objToJson(targetContext));

            var generalContext = new HashMap<String, Object>();

            generalContext.put("targetApi", targetContextDict);
            generalContext.put("input", input);
            generalContext.put("context", contextDict);

            if (contextDict != null) {
                renderHookTemplates(hook, generalContext, null);
            }

            registerHookEventForDelivery(hook, generalContext);
        }
    }

    // desacoplamento entre modulos commons
    protected abstract String getResponseConditionTemplateName();
    protected abstract Map<String, Object> jsonToMap(String jsonContent);
    protected abstract String objToJson(Object obj);
    protected abstract Object newSummary();
    protected abstract String describeSummary(Object summary, String templateName);
}

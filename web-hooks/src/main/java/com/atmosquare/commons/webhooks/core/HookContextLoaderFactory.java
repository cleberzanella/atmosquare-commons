package com.atmosquare.commons.webhooks.core;

import java.util.List;
import java.util.stream.Collectors;

public class HookContextLoaderFactory implements IHookContextLoaderFactory {
    private List<AbstractHookContextLoader> loaders;

    public HookContextLoaderFactory(List<AbstractHookContextLoader> loaders) {
        this.loaders = loaders;
    }

    public AbstractHookContextLoader forHookType(Object hookType) {
        for (var contextLoader : loaders) {
            if (contextLoader.matchHook(hookType)) {
                return contextLoader;
            }
        }

        throw new IllegalStateException(String.format("Hook para tipo %s não localizado, tipos disponiveis: %s", hookType, loaders.stream().map(cl -> cl.getClass().getSimpleName()).collect(Collectors.joining(","))));
    }
}

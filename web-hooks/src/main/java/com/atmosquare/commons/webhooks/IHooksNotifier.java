package com.atmosquare.commons.webhooks;

public interface IHooksNotifier {
    void notify(Object hookType, Object hookInput);
}

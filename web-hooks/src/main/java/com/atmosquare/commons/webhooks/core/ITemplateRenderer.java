package com.atmosquare.commons.webhooks.core;

//import com.atmosquare.commons.webapi.models.Summary;

public interface ITemplateRenderer {
    String renderTemplate(String template, Object state, Object summary);
}

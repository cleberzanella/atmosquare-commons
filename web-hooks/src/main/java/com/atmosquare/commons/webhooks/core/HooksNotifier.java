package com.atmosquare.commons.webhooks.core;

import com.atmosquare.commons.webhooks.IHooksNotifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

public class HooksNotifier implements IHooksNotifier {

    private static final Logger LOGGER = LoggerFactory.getLogger(HooksNotifier.class);
    private IHookContextLoaderFactory contextLoaderFactory;

    public HooksNotifier(IHookContextLoaderFactory contextLoaderFactory) {
        this.contextLoaderFactory = contextLoaderFactory;
    }

    @Override
    public void notify(Object hookType, Object hookInput) {
        var contextLoader = contextLoaderFactory.forHookType(hookType);

        if (contextLoader == null) {
            String msg = "Não há context loader registrado ou implementado para hooks do tipo: " + hookType + " input: " + hookInput;
            throw new IllegalStateException(msg);
        }

        var hooks = contextLoader.getHooks();
        LOGGER.debug("Notificando evento {} com input {} para {} hooks {}", hookType, hookInput, hooks.size(), String.join(",", hooks.stream().map(h -> String.valueOf(h.getHookId())).collect(Collectors.toList())));

        if (!hooks.isEmpty()) {
            contextLoader.renderHooksAndRegisterForDelivery(hookInput, hooks);
        }
    }
}

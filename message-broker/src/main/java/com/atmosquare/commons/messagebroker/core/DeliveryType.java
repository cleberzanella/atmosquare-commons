package com.atmosquare.commons.messagebroker.core;

public enum DeliveryType {
    POINT_TO_POINT, PUBLISHER_TO_SUBSCRIBERS
}

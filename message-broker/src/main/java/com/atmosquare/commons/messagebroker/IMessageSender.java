package com.atmosquare.commons.messagebroker;

import java.time.Duration;
import java.util.Map;

public interface IMessageSender {

    /**
     * Padrão de projeto publish-subscriber. Entrega a mensagem para N subscribers.
     * @param message
     */
    default void publish(Object message){
        publish(message, null);
    }
    void publish(Object message, Duration delay);

    /**
     * Padrão de projeto produce-consumer ou point-to-point com competing consumers.
     * Entrega a mensagem para apenas um dos consumidores processar.
     * @param message
     */
    default void produce(Object message){
        produce(message, null, null, null);
    }
    void produce(Object message, Duration delay, Integer retryCount, Duration retryDelay);
}

package com.atmosquare.commons.messagebroker.core;

import com.atmosquare.commons.messagebroker.IMessageSenderWithHeaders;
import com.atmosquare.commons.messagebroker.config.MessageBrokerMetadata;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public abstract class AbstractMessageSender implements IMessageSenderWithHeaders {

    protected MessageBrokerMetadata metadata;

    private String tenantId, authId, correlationId;

    public AbstractMessageSender(MessageBrokerMetadata metadata) {
        this.metadata = metadata;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public abstract void produce(Object message, Duration delay, Integer retryCount, Duration retryDelay);

    public abstract void publish(Object message, Duration delay);

    protected <T> List<String> defaultValidation(String methodName, DeliveryType deliveryType, T message, Duration delay, Integer retryCount, Duration retryDelay) {
        if (message == null) {
            return Collections.singletonList("message não pode ser null");
        }

        var messageConfig = metadata.getMessageSendersConfig().stream().filter(mc -> mc.getMessageClass().equals(message.getClass())).findFirst();

        if (!messageConfig.isPresent()) {
            return Collections.singletonList("O tipo " + message.getClass().getName() + " é desconhecido, não está declarado como mensagem.");
        }

        List<String> errors = new ArrayList<String>();

        if (messageConfig.get().getDeliveryType() != deliveryType) {
            errors.add("O tipo de mensagem " + message.getClass().getName() + " não pode ser enviado pelo método " + methodName + " pois é do tipo " + messageConfig.get().getDeliveryType() + ".");
        }

        if ((delay != null || retryDelay != null) && !messageConfig.get().isDelayQueue()) {
            errors.add("O tipo de mensagem " + message.getClass().getName() + " não tem suporte a \"delayed messages\" configurado.");
        }

        return errors;
    }

}

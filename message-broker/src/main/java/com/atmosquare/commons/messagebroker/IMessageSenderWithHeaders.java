package com.atmosquare.commons.messagebroker;

public interface IMessageSenderWithHeaders extends IMessageSender {
    void setTenantId(String tenantId);
    String getTenantId();
    void setCorrelationId(String correlationId);
    String getCorrelationId();
}

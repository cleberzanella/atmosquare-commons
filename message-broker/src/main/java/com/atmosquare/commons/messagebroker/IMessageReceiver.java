package com.atmosquare.commons.messagebroker;

public interface IMessageReceiver<T> {
    void receive(IMessageContext<T> messageContext);
}

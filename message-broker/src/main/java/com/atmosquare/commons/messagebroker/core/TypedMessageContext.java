package com.atmosquare.commons.messagebroker.core;

import com.atmosquare.commons.messagebroker.IMessageContext;
import com.atmosquare.commons.messagebroker.IMessagingSession;

public class TypedMessageContext<T> extends AbstractMessageContext implements IMessageContext<T> {
    public TypedMessageContext(IMessagingSession session) {
        super(session);
    }

    @Override
    public T getMessage() {
        return (T) super.getMessageObject();
    }
}

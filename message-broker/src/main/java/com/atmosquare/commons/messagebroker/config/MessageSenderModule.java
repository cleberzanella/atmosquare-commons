package com.atmosquare.commons.messagebroker.config;

import com.atmosquare.commons.messagebroker.core.DeliveryType;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public abstract class MessageSenderModule {
    private String moduleName;
    private List<MessageSenderConfig> configs;

    protected abstract void configure();

    protected <T> MessageSenderConfig publisher(Class<T> messageClass/*, String customMessageName*/) {
        var config = new MessageSenderConfig();
        config.setDeliveryType(DeliveryType.PUBLISHER_TO_SUBSCRIBERS);

        DefaultSet(config, null/*customMessageName*/, messageClass);

        configs.add(config);

        return config;
    }

    protected <T> MessageSenderRetryConfig producer(Class<T> messageClass/*String customMessageName*/) {
        var config = new MessageSenderRetryConfig();
        config.setDeliveryType(DeliveryType.POINT_TO_POINT);

        DefaultSet(config, null/*customMessageName*/, messageClass);

        configs.add(config);

        return config;
    }

    private void DefaultSet(MessageSenderConfig config, String customMessageName, Class<?> messageType) {
        config.setMessageClass(messageType);

        if (getModuleName() == null ||  getModuleName().isEmpty()) {
            config.setModuleName(messageType.getPackage().getName());
        } else {
            config.setModuleName(getModuleName());
        }

        if (customMessageName == null || customMessageName.isEmpty()) {
            config.setMessageName(messageType.getSimpleName());
        } else {
            config.setMessageName(customMessageName);
        }

    }

    public List<MessageSenderConfig> LoadConfigs() {
        configs = new ArrayList<>();

        configure();

        return configs;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public static class MessageSenderConfig {
        private String moduleName;
        private String messageName;

        private Class<?> messageClass;
        private DeliveryType deliveryType;
        private boolean delayQueue;
        private Duration delayTime;
        private int exponentialBackoff;

        private String declaringClassName;

        public MessageSenderConfig Delayed(Duration delay) {
            setDelayQueue(true);
            setDelayTime(delay);

            return this;
        }

        public String getModuleName() {
            return moduleName;
        }

        public void setModuleName(String moduleName) {
            this.moduleName = moduleName;
        }

        public String getMessageName() {
            return messageName;
        }

        public void setMessageName(String messageName) {
            this.messageName = messageName;
        }

        public Class<?> getMessageClass() {
            return messageClass;
        }

        public void setMessageClass(Class<?> messageClass) {
            this.messageClass = messageClass;
        }

        public DeliveryType getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(DeliveryType deliveryType) {
            this.deliveryType = deliveryType;
        }

        public boolean isDelayQueue() {
            return delayQueue;
        }

        public void setDelayQueue(boolean delayQueue) {
            this.delayQueue = delayQueue;
        }

        public Duration getDelayTime() {
            return delayTime;
        }

        public void setDelayTime(Duration delayTime) {
            this.delayTime = delayTime;
        }

        public String getDeclaringClassName() {
            return declaringClassName;
        }

        public void setDeclaringClassName(String declaringClassName) {
            this.declaringClassName = declaringClassName;
        }
    }

    public static class MessageSenderRetryConfig extends MessageSenderConfig {
        private Integer retryCount;
        private Duration retryDelayTime;
        private boolean erroStore;

        public MessageSenderRetryConfig Delayed(Duration delay) {
            return (MessageSenderRetryConfig) super.Delayed(delay);
        }

        public MessageSenderRetryConfig retry(Integer count, Duration delay) {
            setDelayQueue(true);
            setRetryCount(count);
            setRetryDelayTime(delay);

            return this;
        }

        public MessageSenderRetryConfig StoreErrors() {
            setErroStore(true);

            return this;
        }

        public Integer getRetryCount() {
            return retryCount;
        }

        public void setRetryCount(Integer retryCount) {
            this.retryCount = retryCount;
        }

        public Duration getRetryDelayTime() {
            return retryDelayTime;
        }

        public void setRetryDelayTime(Duration retryDelayTime) {
            this.retryDelayTime = retryDelayTime;
        }

        public boolean isErroStore() {
            return erroStore;
        }

        public void setErroStore(boolean erroStore) {
            this.erroStore = erroStore;
        }
    }
}

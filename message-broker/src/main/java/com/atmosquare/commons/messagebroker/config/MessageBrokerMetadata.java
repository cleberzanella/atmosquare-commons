package com.atmosquare.commons.messagebroker.config;

import com.atmosquare.commons.core.inject.IDependencyRegister;
import com.atmosquare.commons.messagebroker.core.DeliveryType;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MessageBrokerMetadata {

    private List<MessageSenderModule.MessageSenderConfig> messageSendersConfig;

    private List<MessageReceiverModule.MessageReceiverConfig> messageReceiversConfig;

    public MessageBrokerMetadata() {

    }

    public List<MessageSenderModule.MessageSenderConfig> getMessageSendersConfig() {
        return messageSendersConfig;
    }

    public List<MessageReceiverModule.MessageReceiverConfig> getMessageReceiversConfig() {
        return messageReceiversConfig;
    }

    public String exchangeNameForMessage(Class<?> messageType) {
        var config = messageSendersConfig.stream().filter(cfg -> cfg.getMessageClass().equals(messageType)).findFirst();

        if (!config.isPresent()) {
            return null;
        }

        String suffix = messageSuffix(config.get().getDeliveryType());

        return "ex_" + config.get().getModuleName() + "." + config.get().getMessageName() + suffix;
    }

    private static String messageSuffix(DeliveryType deliveryType) {
        return deliveryType == DeliveryType.PUBLISHER_TO_SUBSCRIBERS ? "_pubsub" : "_prodcons";
    }

    public String exchangeDelayedNameForMessage(Class<?> messageType) {
        String exName = exchangeNameForMessage(messageType);

        if (exName == null) {
            return null;
        }

        return exName + "_delayed";
    }

    public boolean hasDelayedMessageSupport(Class<?> messageType) {
        return messageSendersConfig.stream().anyMatch(cfg -> cfg.getMessageClass().equals(messageType) && (cfg.getDelayTime() != null || (cfg instanceof MessageSenderModule.MessageSenderRetryConfig && ((MessageSenderModule.MessageSenderRetryConfig) cfg).getRetryDelayTime() != null)));
    }

    public String exchangeErrorNameForMessage(Class<?> messageType) {
        String exName = exchangeNameForMessage(messageType);

        if (exName == null) {
            return null;
        }

        return exName + "_error";
    }

    public String queueNameForMessage(Class<?> messageType) {
        var config = messageSendersConfig.stream().filter(cfg -> cfg.getMessageClass().equals(messageType)).findFirst();

        if (!config.isPresent()) {
            return null;
        }

        String suffix = messageSuffix(config.get().getDeliveryType());

        return "qe_" + config.get().getModuleName() + "." + config.get().getMessageName() + suffix;
    }

    public String queueNameForMessageForReceiver(Class<?> messageType) {
        String queueName = queueNameForMessage(messageType);

        var cfg = messageReceiversConfig.stream().filter(recCfg -> recCfg.getMessageClass() == messageType).findFirst();

        if (!cfg.isPresent() || (cfg.get().getReceiverModuleName() != null && !cfg.get().getReceiverModuleName().isEmpty())) {
            return null;
        }

        return queueName + "_" + cfg.get().getReceiverModuleName();
    }

    public String queueDelayedNameForMessage(Class<?> messageType) {
        String exName = queueNameForMessage(messageType);

        if (exName == null) {
            return null;
        }

        return exName + "_delayed";
    }

    public MessageSenderModule.MessageSenderConfig configByExchangeName(String exchangeName) {
        var it = messageSendersConfig.stream().filter(cfg -> exchangeName.startsWith(exchangeNameForMessage(cfg.getMessageClass()))).findFirst();
        if (it.isPresent()) {
            return it.get();
        }

        return null;
    }

    public void loadSenders(List<Class<?>> senderModuleTypes) {
        List<String> allErrors = new ArrayList<>();
        List<MessageSenderModule.MessageSenderConfig> senderCfgs = new ArrayList<>();

        for (Class<?> senderModuleType : senderModuleTypes) {

            List<String> classErrors = new ArrayList<>();

            boolean isConcrete = !Modifier.isAbstract(senderModuleType.getModifiers()) && !senderModuleType.isInterface();
            if (!isConcrete) {
                classErrors.add("O módulo " + senderModuleType.getName() + " deve ser uma classe concreta pública e não abstrata.");
            }

            boolean isSenderModule = MessageSenderModule.class.isAssignableFrom(senderModuleType);
            if (!isSenderModule) {
                classErrors.add("A classe " + senderModuleType.getName() + " não é um módulo esperado,  deve estender " + MessageSenderModule.class.getName() + ".");
            }

            Constructor<?> parameterlessConstructor = null;
            try {
                parameterlessConstructor = senderModuleType.getConstructor();
            } catch (NoSuchMethodException e) {
                //
            }
            if (parameterlessConstructor == null) {
                classErrors.add("A classe " + senderModuleType.getName() + " precisa ter um construtor público e sem parâmetros para ser instanciada.");
            }

            if (!classErrors.isEmpty()) {
                allErrors.addAll(classErrors);
                continue;
            }

            MessageSenderModule senderModuleInstance = null;
            try {
                senderModuleInstance = (MessageSenderModule) parameterlessConstructor.newInstance();
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException exc) {
                allErrors.add("A classe " + senderModuleType.getName() + " não pôde ser instanciada: " + exc.getMessage());
                continue;
            }

            var configs = senderModuleInstance.LoadConfigs();
            for (var cfg : configs) {
                cfg.setDeclaringClassName(senderModuleType.getName());
            }
            senderCfgs.addAll(configs);

        }

        this.messageSendersConfig = new ArrayList<>(senderCfgs);

        if (!allErrors.isEmpty()) {
            throw new IllegalArgumentException("Foram encontradas " + allErrors.size() + " falhas na configuração das mensagens: " + String.join("\n", allErrors));
        }

    }

    public void loadReceivers(List<Class<?>> receiverModuleTypes) {
        List<String> allErrors = new ArrayList<>();
        List<MessageReceiverModule.MessageReceiverConfig> receiverCfgs = new ArrayList<>();

        for (Class<?> receiverModuleType : receiverModuleTypes) {

            List<String> classErrors = new ArrayList<>();

            boolean isConcrete = !Modifier.isAbstract(receiverModuleType.getModifiers()) && !receiverModuleType.isInterface();
            if (!isConcrete) {
                classErrors.add("O módulo " + receiverModuleType.getName() + " deve ser uma classe concreta pública e não abstrata.");
            }

            boolean isReceiverModule = MessageReceiverModule.class.isAssignableFrom(receiverModuleType);
            if (!isReceiverModule) {
                classErrors.add("A classe " + receiverModuleType.getName() + " não é um módulo esperado,  deve estender " + MessageReceiverModule.class.getName() + ".");
            }

            Constructor<?> parameterlessConstructor = null;
            try {
                parameterlessConstructor = receiverModuleType.getConstructor();
            } catch (NoSuchMethodException e) {
                //
            }
            if (parameterlessConstructor == null) {
                classErrors.add("A classe " + receiverModuleType.getName() + " precisa ter um construtor público e sem parâmetros para ser instanciada.");
            }

            if (!classErrors.isEmpty()) {
                allErrors.addAll(classErrors);
                continue;
            }

            MessageReceiverModule receiverModuleInstance = null;
            try {
                receiverModuleInstance = (MessageReceiverModule) parameterlessConstructor.newInstance();
            } catch (InvocationTargetException | InstantiationException | IllegalAccessException exc) {
                allErrors.add("A classe " + receiverModuleType.getName() + " não pôde ser instanciada: " + exc.getMessage());
                continue;
            }

            var configs = receiverModuleInstance.LoadConfigs();
            for (var cfg : configs) {
                cfg.setDeclaringClassName(receiverModuleType.getName());
            }
            receiverCfgs.addAll(configs);

        }

        this.messageReceiversConfig = new ArrayList<>(receiverCfgs);

        if (!allErrors.isEmpty()) {
            throw new IllegalArgumentException("Foram encontradas " + allErrors.size() + " falhas na configuração das mensagens: " + String.join("\n", allErrors));
        }
    }

    public void validateAndThrow() {
        var failures = validateInternal(messageSendersConfig, messageReceiversConfig);

        if (!failures.isEmpty()) {
            throw new IllegalArgumentException("Foram encontradas " + failures.size() + " incosistências na configuração de mensageria: " + String.join("\n", failures));
        }

    }

    protected List<String> validateInternal(List<MessageSenderModule.MessageSenderConfig> senderConfigs, List<MessageReceiverModule.MessageReceiverConfig> receiverConfigs) {
        List<String> allErrors = new ArrayList<>();

        var orderedMessageConfigs = senderConfigs.stream().sorted(Comparator.comparing(MessageSenderModule.MessageSenderConfig::getModuleName).thenComparing(MessageSenderModule.MessageSenderConfig::getMessageName)).collect(Collectors.toList());
        for (var MessageSenderModule : orderedMessageConfigs) {

            String errorStr = "";

            // declaração duplicada da mensagem
            var msgDcl = senderConfigs.stream().filter(cfg -> cfg.getMessageClass() == MessageSenderModule.getMessageClass()).map(cfg -> cfg.getDeclaringClassName()).collect(Collectors.toList());
            if (msgDcl.size() > 1) {
                errorStr += "\nDeclaração de mensagem duplicada em: " + String.join(", ", msgDcl);
            }

            if (receiverConfigs != null) {
                var csmDcl = receiverConfigs.stream().filter(cfg -> cfg.getMessageClass() == MessageSenderModule.getMessageClass()).map(cfg -> cfg.getDeclaringClassName()).collect(Collectors.toList());
                if (MessageSenderModule.getDeliveryType() == DeliveryType.POINT_TO_POINT) {
                    // mais de um consumer para mensagens point-to-point
                    if (csmDcl.size() > 1) {
                        errorStr += "\nDeclaração de mais de um consumidor para mensagem do tipo producer/consumer em: " + String.join(", ", csmDcl);
                    }

                    // mensagem point-to-point com subscriber vinculado
                    var subs = receiverConfigs.stream().filter(cfg -> cfg.getMessageClass() == MessageSenderModule.getMessageClass() && cfg.getDeliveryType() == DeliveryType.PUBLISHER_TO_SUBSCRIBERS).map(cfg -> cfg.getDeclaringClassName()).collect(Collectors.toList());
                    if (subs.size() > 0) {
                        errorStr += "\nMensagem point-to-point não pode ter subscribers vinculados. Módulos incorretos: " + String.join(", ", subs);
                    }
                }

                if (MessageSenderModule.getDeliveryType() == DeliveryType.PUBLISHER_TO_SUBSCRIBERS) {

                    // mensagem pub-sub com consumidor vinculado
                    var cons = receiverConfigs.stream().filter(cfg -> cfg.getMessageClass() == MessageSenderModule.getMessageClass() && cfg.getDeliveryType() == DeliveryType.POINT_TO_POINT).map(cfg -> cfg.getDeclaringClassName()).collect(Collectors.toList());
                    if (cons.size() > 0) {
                        errorStr += "\nMensagem pub-sub não pode ter consumer vinculado. Módulos incorretos: " + String.join(", ", cons);
                    }
                }
            }

            if (!errorStr.isEmpty()) {
                String s = "Mensagem com inconsistências: " + MessageSenderModule.getModuleName() + "." + MessageSenderModule.getMessageName() + " declarado em: " + MessageSenderModule.getDeclaringClassName() + ":\n";
                allErrors.add(s + errorStr);
            }

        }

        // consumidores duplicados
        if (messageReceiversConfig != null) {
            for (var messageReceiverConfig : messageReceiversConfig) {
                String errorStr = "";

                var MessageSenderModule = senderConfigs.stream().filter(scfg -> scfg.getMessageClass() == messageReceiverConfig.getMessageClass()).findFirst();

                if (!MessageSenderModule.isPresent()) {
                    errorStr += "\nProcessamento cadastrado para classe que não está declarada como mensagem: " + messageReceiverConfig.getMessageClass().getName() + " declarado em " + messageReceiverConfig.getDeclaringClassName();
                } else if (messageReceiverConfig.getDeliveryType() == DeliveryType.POINT_TO_POINT) {

                    var msgDcl = messageReceiversConfig.stream().filter(cfg -> cfg.getMessageClass() == messageReceiverConfig.getMessageClass()).map(cfg -> cfg.getDeclaringClassName()).collect(Collectors.toList());
                    if (msgDcl.size() > 1) {
                        errorStr += "\nDeclaração de processamento duplicado não é permitido para mensagem do tipo point-to-point em: " + String.join(", ", msgDcl);
                    }

                }

                if (!errorStr.isEmpty()) {
                    String s = "Processamento com inconsistências: " + messageReceiverConfig.getReceiverClass().getName() + " declarado em: " + messageReceiverConfig.getDeclaringClassName() + ":\n";
                    allErrors.add(s + errorStr);
                }
            }
        }

        return allErrors;
    }

    public void installReceivers(IDependencyRegister dependencyRegister) {
        if (getMessageReceiversConfig() == null) {
            return;
        }

        for (var messageReceiverConfig : getMessageReceiversConfig()) {
            dependencyRegister.registerUntyped(messageReceiverConfig.getReceiverClass(), messageReceiverConfig.getReceiverClass());
        }
    }

}

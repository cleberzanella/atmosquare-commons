package com.atmosquare.commons.messagebroker;

import java.util.Map;

public interface IMessageContextWithHeaders {
    Map<String, Object> getHeaders();
}

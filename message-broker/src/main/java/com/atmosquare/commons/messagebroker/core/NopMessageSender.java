package com.atmosquare.commons.messagebroker.core;

import com.atmosquare.commons.messagebroker.IMessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Map;

public class NopMessageSender implements IMessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(NopMessageSender.class);

    @Override
    public void publish(Object message, Duration delay) {
        LOGGER.warn("Mensagem perdida: " + message);
    }

    @Override
    public void produce(Object message, Duration delay, Integer retryCount, Duration retryDelay) {
        LOGGER.warn("Mensagem perdida: " + message);
    }
}

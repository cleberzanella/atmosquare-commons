package com.atmosquare.commons.messagebroker;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class MessageArgsBuilder {

    private Map<String, Object> headers = new HashMap<>();

    public MessageArgsBuilder delayedDelivery(Duration delay) {
        headers.put("x-delivery-delayed", String.valueOf(delay.get(ChronoUnit.MILLIS)));
        return this;
    }

    public MessageArgsBuilder retryCount(int count) {
        headers.put("x-retry-count", String.valueOf(count));
        return this;
    }

    public MessageArgsBuilder delayedRetry(Duration delay) {
        headers.put("x-retry-delayed", String.valueOf(delay.get(ChronoUnit.MILLIS)));
        return this;
    }

    public Map<String, Object> create() {
        return headers;
    }
}

package com.atmosquare.commons.messagebroker;

import com.atmosquare.commons.messagebroker.core.AbstractMessageContext;

import java.io.Closeable;

public interface IMessagingSession extends Closeable {
    Object getNativeSession();
    void ack(AbstractMessageContext context);
    void nack(AbstractMessageContext context);
    IMessageSender createMessageSender(IMessageContextWithHeaders context);

    // especifico do rabbitmq
    Object createBasicProperties();
    void basicPublish(String exchange, String routingKey, Object basicProperties, byte[] body);
}

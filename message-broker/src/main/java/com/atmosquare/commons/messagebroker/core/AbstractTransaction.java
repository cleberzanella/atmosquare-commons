package com.atmosquare.commons.messagebroker.core;

import com.atmosquare.commons.core.storage.ITransaction;

import java.util.List;

public abstract class AbstractTransaction implements ITransaction {

    protected List<Object> internalOperations;

    private boolean active;

    private boolean committed;

    private boolean rolledback;

    @Override
    public boolean isActive() {
        return active;
    }

    protected void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public boolean wasCommitted() {
        return committed;
    }

    protected void setCommitted(boolean committed) {
        this.committed = committed;
    }

    @Override
    public boolean wasRolledBack() {
        return rolledback;
    }

    protected void setRolledback(boolean rolledback) {
        this.rolledback = rolledback;
    }

    public abstract void setOnChange(TransactionChange changeEvent);

    public abstract void commit();

    public abstract void rollback();

    public abstract void close();
}

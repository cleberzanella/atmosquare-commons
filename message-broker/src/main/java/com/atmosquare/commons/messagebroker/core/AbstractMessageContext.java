package com.atmosquare.commons.messagebroker.core;

import com.atmosquare.commons.core.auth.IAuthInfo;
import com.atmosquare.commons.core.auth.IAuthInfoProvider;
import com.atmosquare.commons.messagebroker.IMessageContextWithHeaders;
import com.atmosquare.commons.messagebroker.IMessageSender;
import com.atmosquare.commons.messagebroker.IMessagingSession;

import java.io.Closeable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AbstractMessageContext implements IMessageContextWithHeaders, IAuthInfoProvider {

    private IMessagingSession session;
    private List<Closeable> disposes = new ArrayList<>();
    private IMessageSender sender;

    private boolean acked;
    private boolean nacked;

    private Object messageObject;
    private Map<String, Object> headers;

    private OnNackListener onNackAction;

    private IAuthInfo authInfo;

    protected AbstractMessageContext(IMessagingSession session) {
        this.session = session;
    }

    @Override
    public IAuthInfo getAuthInfo() {
        return authInfo;
    }

    @Override
    public void setAuthInfo(IAuthInfo authInfo) {
        this.authInfo = authInfo;
    }

    public boolean isAcked() {
        return acked;
    }

    public void setAcked(boolean acked) {
        this.acked = acked;
    }

    public boolean isNacked() {
        return nacked;
    }

    public void setNacked(boolean nacked) {
        this.nacked = nacked;
    }

    public void setOnNackAction(OnNackListener onNackAction) {
        this.onNackAction = onNackAction;
    }

    public IMessageSender getSender() {
        return sender;
    }

    public void setSender(IMessageSender sender) {
        this.sender = sender;
//        if(sender intanceof IAuthInfoProvider)
//        {
//            ((IAuthInfoProvider) sender).AuthInfo = this.AuthInfo;
//        }
    }

    public Object getMessageObject() {
        return messageObject;
    }

    public void setMessageObject(Object messageObject) {
        this.messageObject = messageObject;
    }

    @Override
    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, Object> headers) {
        this.headers = headers;
    }

    public void ack() {
        session.ack(this);
        setAcked(true);
    }

    public void nack() {
        if (onNackAction != null) {
            onNackAction.nack(this);
        }

        if (!isAcked() && !isNacked()) {
            session.nack(this);
            setNacked(true);
        }
    }

    public void registerForDispose(Closeable disposable) {
        disposes.add(disposable);
    }

    public void close() {

        try {
            session.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FunctionalInterface
    public interface OnNackListener {
        void nack(AbstractMessageContext context);
    }
}

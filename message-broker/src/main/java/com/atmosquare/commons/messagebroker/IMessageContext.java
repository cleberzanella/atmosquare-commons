package com.atmosquare.commons.messagebroker;

import java.io.Closeable;

public interface IMessageContext<T> extends IMessageContextWithHeaders, AutoCloseable {

    T getMessage();
    IMessageSender getSender();

    void ack();
    void nack();
    boolean isAcked();
    boolean isNacked();

    void registerForDispose(Closeable closeable);

}

package com.atmosquare.commons.messagebroker.config;

import com.atmosquare.commons.messagebroker.IMessageReceiver;
import com.atmosquare.commons.messagebroker.core.DeliveryType;

import java.util.ArrayList;
import java.util.List;

public abstract class MessageReceiverModule {

    private String moduleName;
    private List<MessageReceiverConfig> configs = new ArrayList<>();

    protected abstract void configure();

    protected <T, TR extends IMessageReceiver<T>> void consumer(Class<T> messageClass, Class<TR> messageReceiverClass) {
        var cfg = new MessageReceiverConfig();
        cfg.setDeliveryType(DeliveryType.POINT_TO_POINT);
        cfg.setMessageClass(messageClass);
        cfg.setReceiverClass(messageReceiverClass);

        configs.add(cfg);
    }

    protected <T, TR extends IMessageReceiver<T>> void Subscriber(Class<T> messageClass, Class<TR> messageReceiverClass) {
        var cfg = new MessageReceiverConfig();
        cfg.setDeliveryType(DeliveryType.PUBLISHER_TO_SUBSCRIBERS);
        cfg.setMessageClass(messageClass);
        cfg.setReceiverClass(messageReceiverClass);

        configs.add(cfg);
    }

    public List<MessageReceiverConfig> LoadConfigs() {
        configs = new ArrayList<>();

        configure();

        return configs;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

// TODO suporte a exponential backoff ?
    public static class MessageReceiverConfig {
        private Class<?> messageClass;
        private DeliveryType deliveryType;
        private Class<?> receiverClass;

        private boolean ignored;

        private String declaringClassName;
        private String receiverModuleName;

        public Class<?> getMessageClass() {
            return messageClass;
        }

        public void setMessageClass(Class<?> messageClass) {
            this.messageClass = messageClass;
        }

        public com.atmosquare.commons.messagebroker.core.DeliveryType getDeliveryType() {
            return deliveryType;
        }

        public void setDeliveryType(com.atmosquare.commons.messagebroker.core.DeliveryType deliveryType) {
            this.deliveryType = deliveryType;
        }

        public Class<?> getReceiverClass() {
            return receiverClass;
        }

        public void setReceiverClass(Class<?> receiverClass) {
            this.receiverClass = receiverClass;
        }

        public boolean isIgnored() {
            return ignored;
        }

        public void setIgnored(boolean ignored) {
            this.ignored = ignored;
        }

        public String getDeclaringClassName() {
            return declaringClassName;
        }

        public void setDeclaringClassName(String declaringClassName) {
            this.declaringClassName = declaringClassName;
        }

        public String getReceiverModuleName() {
            return receiverModuleName;
        }

        public void setReceiverModuleName(String receiverModuleName) {
            this.receiverModuleName = receiverModuleName;
        }
    }

}

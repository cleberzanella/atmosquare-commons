package com.atmosquare.commons.msexcel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.IOException;
import java.io.InputStream;

public class ExcelUtils {

    public static Workbook openExcelStream(InputStream inputStream){
        try {
            return WorkbookFactory.create(inputStream);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } catch (InvalidFormatException e) {
            throw new IllegalArgumentException(e);
        }
    }

}

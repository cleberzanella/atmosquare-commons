package com.atmosquare.commons.msexcel.tests;

import com.atmosquare.commons.msexcel.ExcelUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ExcelUtilsTests {

    public static final String SAMPLE_XLS_FILE_PATH = "./sample-xls-file.xls";
    public static final String SAMPLE_XLSX_FILE_PATH = "./sample-xlsx-file.xlsx";

    // https://github.com/callicoder/java-read-write-excel-file-using-apache-poi
    @Test
    void documentCompose() {

        InputStream is = ExcelUtils.class.getResourceAsStream("/excel-sample/sample-xlsx-file.xlsx");

        Workbook workbook = ExcelUtils.openExcelStream(is);

        assertNotNull(workbook);
    }

}

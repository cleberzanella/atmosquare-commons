package com.atmosquare.commons.xml;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.CollationElementIterator;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class XmlUtils {

    public static Document copyDocument(Document sourceDoc){
        Document copyDoc = newDocument();
        Node xmlCopy = copyDoc.importNode(sourceDoc.getDocumentElement(), true);
        copyDoc.appendChild(xmlCopy);
        return copyDoc;
    }

    public static Document fromString(String xmlContent){
        return fromString(DocumentBuilderFactory.newInstance(), xmlContent);
    }

    public static Document fromString(DocumentBuilderFactory builderFactory, String xmlContent){

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(); //dbf.setNamespaceAware(true);
        DocumentBuilder dBuilder = newDocumentBuilder(builderFactory);

        InputSource is = new InputSource(new StringReader(xmlContent));
        try {
            return dBuilder.parse(is);
        } catch (SAXException | IOException e) {
            throw new IllegalArgumentException("Error parsing xml content", e);
        }
    }

    public static Document newDocument(){
        return newDocumentBuilder().newDocument();
    }

    public static DocumentBuilder newDocumentBuilder(){
        return newDocumentBuilder(DocumentBuilderFactory.newInstance());
    }

    public static DocumentBuilder newDocumentBuilder(DocumentBuilderFactory dbFactory) {

        try {
            return dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new IllegalStateException("Error at XML reader creation", e);
        }

    }

    public static String toIndentedString(Node document){

        Map<String, String> outputProperties = new HashMap<>();

        outputProperties.put(OutputKeys.INDENT, "yes");
        //outputProperties.put(OutputKeys.OMIT_XML_DECLARATION, "yes");
        outputProperties.put("{http://xml.apache.org/xslt}indent-amount", "2");

        return toString(document, outputProperties);
    }

    public static String toString(Node document){
        return toString(document, Collections.<String, String>emptyMap());
    }

    public static String toStringOmitingXmlDeclaration(Node document){
        return toString(document, Collections.singletonMap(OutputKeys.OMIT_XML_DECLARATION, "yes"));
    }

    public static String toString(Node document, Map<String, String> outputProperties){

        Transformer transformer;
        try {
            transformer = TransformerFactory.newInstance().newTransformer();

            for(Map.Entry<String, String> entry : outputProperties.entrySet()){
                transformer.setOutputProperty(entry.getKey(), entry.getValue());
            }

        } catch (TransformerConfigurationException e) {
            throw new IllegalStateException(e);
        }

        //initialize StreamResult with File object to save to file
        StreamResult result = new StreamResult(new StringWriter());
        DOMSource source = new DOMSource(document);
        try {
            transformer.transform(source, result);
        } catch (TransformerException e) {
            throw new IllegalStateException(e);
        }

        return result.getWriter().toString();

    }

    public static void toFile(Document document, File targetFile){

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            throw new IllegalStateException(e);
        }
        DOMSource domSource = new DOMSource(document);
        StreamResult streamResult = new StreamResult(targetFile);

        try {
            transformer.transform(domSource, streamResult);
        } catch (TransformerException e) {
            throw new IllegalStateException(e);
        }
    }
}
package com.atmosquare.commons.xml;

import org.w3c.dom.Document;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NamespaceResolver implements NamespaceContext
{
    private Document sourceDocument;
    private Map<String, String> prefixNamespaceMap;

    public NamespaceResolver(Document document) {
        sourceDocument = document;
        this.prefixNamespaceMap = new HashMap<>();
    }

    public void addNamespace(String prefix, String namespaceUri){
        prefixNamespaceMap.put(prefix, namespaceUri);
    }

    public String getNamespaceURI(String prefix) {
        if (prefix.equals(XMLConstants.DEFAULT_NS_PREFIX)) {
            return sourceDocument.lookupNamespaceURI(null);
        } else {
            String namespaceUri = sourceDocument.lookupNamespaceURI(prefix);

            if(namespaceUri != null){
                return namespaceUri;
            }

            return prefixNamespaceMap.get(prefix);
        }
    }

    public String getPrefix(String namespaceURI) {
        return sourceDocument.lookupPrefix(namespaceURI);
    }

    @SuppressWarnings("rawtypes")
    public Iterator getPrefixes(String namespaceURI) {
        return null;
    }
}
package com.atmosquare.commons.xml;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XmlBuilderUtils {

    private enum CreatePosition {
        BEFORE,
        AFTER
    }

    public static Node addValueByXPath(Document xmlDocument, Node xmlStartNode, String xpath, String stringValue){

        NodePair nodePair = getOrCreateNode(xmlDocument, xmlStartNode, xpath);

        nodePair.valueNode.setTextContent(stringValue);
        return nodePair.valueNode;
    }

    public static Node addValueBeforeByXPath(Document xmlDocument, Node xmlStartNode,String positionRefPath,  String xpath, String stringValue){
        return addValuePositionalByXPath(xmlDocument, xmlStartNode, positionRefPath, xpath, stringValue, CreatePosition.BEFORE);
    }

    public static Node addValueAfterByXPath(Document xmlDocument, Node xmlStartNode,String positionRefPath,  String xpath, String stringValue){
        return addValuePositionalByXPath(xmlDocument, xmlStartNode, positionRefPath, xpath, stringValue, CreatePosition.AFTER);
    }

    protected static Node addValuePositionalByXPath(Document xmlDocument, Node xmlStartNode,String positionRefPath,  String xpath, String stringValue, CreatePosition position){

        NodePair afterNodePair = getOrCreateNode(xmlDocument, xmlStartNode, positionRefPath);

        Node parentNode = afterNodePair.valueNode.getParentNode();

        NodePair newNode = getOrCreateNode(xmlDocument, parentNode, xpath);
        parentNode.removeChild(newNode.mainNode);

        switch (position){
            case BEFORE:
                parentNode.insertBefore(newNode.mainNode, afterNodePair.valueNode);
                break;
            case AFTER:
                // .getNextSibling() não funciona igual ao Java no Android
                //parentNode.insertBefore(newNode.mainNode, afterNodePair.valueNode.getNextSibling());
                if(parentNode.getLastChild() == afterNodePair.valueNode){
                    parentNode.appendChild(newNode.mainNode);
                } else {
                    NodeList childNodes = parentNode.getChildNodes();
                    int refIndex = 0;
                    for(int i = 0; i < childNodes.getLength(); i++){
                        refIndex = i;
                        if(childNodes.item(i) == afterNodePair.valueNode){
                            break;
                        }
                    }

                    Node refNode = childNodes.item(refIndex + 1);
                    parentNode.insertBefore(newNode.mainNode, refNode);
                }
                break;
            default:
                throw new IllegalArgumentException(String.format("Invallid position: %s", position));
        }

        newNode.valueNode.setTextContent(stringValue);
        return newNode.valueNode;
    }

    protected static NodePair getOrCreateNode(Document xmlDocument, Node xmlStartNode, String xpath) {

        Node mainNode = null;
        Node lastNode = xmlStartNode;

        // xml segments
        // Example value "/root/element1/element1-child/name"
        // Example value "/root/element1/element1-child/@id"

        xpath = xpath.replaceFirst("/", "");
        String[] paths = xpath.split("/");

        for(String nextNodePath : paths){

            // atributo
            if(nextNodePath.startsWith("@")){

                String attributeName = nextNodePath.replaceFirst("@", "");
                Node attributeNode = lastNode.getAttributes().getNamedItem(attributeName);

                if(attributeNode == null){
                    Attr attr = xmlDocument.createAttribute(attributeName);
                    ((Element) lastNode).setAttributeNode(attr);
                    attributeNode = attr;
                }

                lastNode = attributeNode;
            } else {
                // nó

                Node node = null;

                boolean isArrayItem = nextNodePath.endsWith("]");
                int arrayIndex = 0;
                if(isArrayItem){
                    arrayIndex = Integer.parseInt(nextNodePath.substring(nextNodePath.indexOf("[") + 1, nextNodePath.length() - 1));
                    nextNodePath = nextNodePath.substring(0,nextNodePath.indexOf("["));

                    if(arrayIndex <= 0){
                        throw new IllegalArgumentException(String.format("Illegal array index at path: %s.", xpath));
                    }

                    arrayIndex--;
                }

                int indexCount = 0;
                for(int i = 0; i < lastNode.getChildNodes().getLength(); i++){
                    Node childNode = lastNode.getChildNodes().item(i);
                    if(childNode.getNodeName().equals(nextNodePath) && indexCount == arrayIndex){
                        node = childNode;
                        break;
                    } else
                    if(childNode.getNodeName().equals(nextNodePath)){
                        indexCount++;
                    }
                }

                if(node == null){
                    Node newNode = xmlDocument.createElement(nextNodePath);
                    node = newNode;

                    lastNode.appendChild(newNode);
                }

                if(mainNode == null){
                    mainNode = node;
                }

                lastNode = node;
            }

        }

        return new NodePair(mainNode, lastNode);
    }

    private static class NodePair {

        public Node mainNode;
        public Node valueNode;

        public NodePair(Node mainNode, Node valueNode) {
            this.mainNode = mainNode;
            this.valueNode = valueNode;
        }
    }

    public static void renameByXPath(Document xmlDocument, Node xmlStartNode, String xpath, String nodeNewName) {

        NodePair nodePair = getOrCreateNode(xmlDocument, xmlStartNode, xpath);
        xmlDocument.renameNode(nodePair.valueNode, null, nodeNewName);

    }

    public static void removeByXPath(Document xmlDocument, String xpath) {

        NodeList nodeList = XPathUtils.getNodeList(xmlDocument, xpath);

        for(int i = 0; i < nodeList.getLength(); i++){
            Node node = nodeList.item(i);

            Element parentNode;

            if(node instanceof Attr){
                parentNode = ((Attr) node).getOwnerElement();
                parentNode.removeAttributeNode((Attr) node);
            } else {
                parentNode = (Element) node.getParentNode();
                parentNode.removeChild(node);
            }

        }

    }

    public static void removeOtherChildrenByXPath(Node xmlStartNode, String xpath, String... childrenNames) {

        List<String> childrenNamesList = Arrays.asList(childrenNames);

        Node parentNode = XPathUtils.getNode(xmlStartNode, xpath);

        NodeList childrenList = parentNode.getChildNodes();
        List<Node> toRemove = new ArrayList<>();
        for(int i = 0; i < childrenList.getLength(); i++){
            Node child = childrenList.item(i);

            if(childrenNamesList.contains( child.getNodeName() )){
                continue;
            }

            toRemove.add(child);
        }

        for(Node node : toRemove){
            parentNode.removeChild(node);
        }

    }

}
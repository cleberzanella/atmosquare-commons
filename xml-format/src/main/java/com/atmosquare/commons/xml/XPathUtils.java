package com.atmosquare.commons.xml;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.Locale;

public class XPathUtils {

    public static String getString(Node xmlItem, String xpath){
        return getString(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static String getString(Node xmlItem, XPathFactory xPathfactory, String xpath){
        // TODO retorna vazio se não existir
        return (String) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.STRING);
    }

    protected static Number getNumber(Node xmlItem, XPathFactory xPathfactory, String xpath){
        return (Number) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.NUMBER);
    }

    public static Double getDouble(Node xmlItem, String xpath){
        return getDouble(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static Double getDouble(Node xmlItem, XPathFactory xPathfactory, String xpath){
        Number number = getNumber(xmlItem, xPathfactory, xpath);
        return number == null ? null : number.doubleValue();
    }

    public static Integer getInteger(Node xmlItem, String xpath){
        return getInteger(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static Integer getInteger(Node xmlItem, XPathFactory xPathfactory, String xpath){
        Number number = getNumber(xmlItem, xPathfactory, xpath);

        if(number == null){
            return null;
        }

        if(number.equals(Double.NaN)){
            return null;
        }

        return number.intValue();
    }

    public static Long getLong(Node xmlItem, String xpath){
        return getLong(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static Long getLong(Node xmlItem, XPathFactory xPathfactory, String xpath){
        Number number = getNumber(xmlItem, xPathfactory, xpath);

        if(number == null){
            return null;
        }

        if(number.equals(Double.NaN)){
            return null;
        }

        return number.longValue();
    }

    public static BigDecimal getDecimal(Node xmlItem, String xpath){
        return getDecimal(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static BigDecimal getDecimalOrElse(Node xmlItem, String xpath, BigDecimal defaultValue){
        return getDecimalOrElse(xmlItem, XPathFactory.newInstance(), xpath, defaultValue);
    }

    public static BigDecimal getDecimal(Node xmlItem, XPathFactory xPathfactory, String xpath){
        return getDecimalOrElse(xmlItem, XPathFactory.newInstance(), xpath, null);
    }

    public static BigDecimal getDecimalOrElse(Node xmlItem, XPathFactory xPathfactory, String xpath, BigDecimal defaultValue){
        String number = getString(xmlItem, xPathfactory, xpath);
        return number == null || number.isEmpty() ? defaultValue : new BigDecimal(number);
    }

    public static Date getDate(Node xmlItem, String xpath, DateFormat... dateFormats){
        return getDateOrElse(xmlItem, xpath, null, dateFormats);
    }

    public static Date getDateOrElse(Node xmlItem, String xpath, Date defaultValue, DateFormat... dateFormats){

        String dateStr = getString(xmlItem, xpath);

        if(dateStr == null || dateStr.isEmpty()){
            return null;
        }

        for(DateFormat dateFormat : dateFormats){
            try {
                return dateFormat.parse(dateStr);
            } catch (ParseException e) {
                // ignore
            }
        }

        return defaultValue;
    }

    public static Date getIsoDate(Node xmlItem, String xpath){
        SimpleDateFormat ISO8601DATEFORMAT_WITH_MILLIS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ", Locale.US);
        SimpleDateFormat ISO8601DATEFORMAT_WITH_SECONDS = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US);
        SimpleDateFormat ISO8601DATEFORMAT_WITH_MINUTES = new SimpleDateFormat("yyyy-MM-dd'T'HH:mmZ", Locale.US);

        return getDate(xmlItem, xpath, ISO8601DATEFORMAT_WITH_MILLIS, ISO8601DATEFORMAT_WITH_SECONDS, ISO8601DATEFORMAT_WITH_MINUTES);
    }

    public static Instant getUtcInstant(Node xmlItem, String xpath, DateTimeFormatter... dateFormats){
        return getUtcInstantOrElse(xmlItem, xpath, null, dateFormats);
    }

    public static Instant getUtcInstantOrElse(Node xmlItem, String xpath, Instant defaultValue, DateTimeFormatter... dateFormats){

        String dateStr = getString(xmlItem, xpath);

        if(dateStr == null || dateStr.isEmpty()){
            return null;
        }

        for(DateTimeFormatter dateFormat : dateFormats){
            try {
                return Instant.from(dateFormat.parse(dateStr));
            } catch (DateTimeParseException e) {
                // ignore
            }
        }

        return defaultValue;
    }

    public static Instant getIsoUtcInstant(Node xmlItem, String xpath){
        return getUtcInstant(xmlItem, xpath, DateTimeFormatter.ISO_INSTANT);
    }

    public static OffsetDateTime getOffsetDateTime(Node xmlItem, String xpath, DateTimeFormatter... dateFormats){
        return getOffsetDateTimeOrElse(xmlItem, xpath, null, dateFormats);
    }

    public static OffsetDateTime getOffsetDateTimeOrElse(Node xmlItem, String xpath, OffsetDateTime defaultValue, DateTimeFormatter... dateFormats){

        String dateStr = getString(xmlItem, xpath);

        if(dateStr == null || dateStr.isEmpty()){
            return null;
        }

        for(DateTimeFormatter dateFormat : dateFormats){
            try {
                return OffsetDateTime.from(dateFormat.parse(dateStr));
            } catch (DateTimeParseException e) {
                // ignore
            }
        }

        return defaultValue;
    }

    public static Instant getIsoOffsetDateTime(Node xmlItem, String xpath){
        return getUtcInstant(xmlItem, xpath, DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public static boolean testCondition(Node xmlItem, String xpath){
        return testCondition(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static boolean testCondition(Node xmlItem, XPathFactory xPathfactory, String xpath){
        return (Boolean) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.BOOLEAN);
    }

    public static Node getNode(Node xmlItem, String xpath){
        return getNode(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static Node getNode(Node xmlItem, String xpath, NamespaceContext nsContext){
        return getNode(xmlItem, XPathFactory.newInstance(), xpath, nsContext);
    }

    public static Node getNode(Node xmlItem, XPathFactory xPathfactory, String xpath){
        return (Node) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.NODE);
    }

    public static Node getNode(Node xmlItem, XPathFactory xPathfactory, String xpath, NamespaceContext nsContext){
        return (Node) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.NODE, nsContext);
    }

    public static NodeList getNodeList(Node xmlItem, String xpath){
        return getNodeList(xmlItem, XPathFactory.newInstance(), xpath);
    }

    public static NodeList getNodeList(Node xmlItem, XPathFactory xPathfactory, String xpath){
        return (NodeList) getXPath(xmlItem, xPathfactory, xpath, XPathConstants.NODESET);
    }

    public static Object getXPath(Node xmlItem, XPathFactory xPathfactory, String xpath, QName dataType){
        return getXPath(xmlItem, xPathfactory, xpath, dataType, null);
    }

    public static Object getXPath(Node xmlItem, XPathFactory xPathfactory, String xpath, QName dataType, NamespaceContext nsContext){
        try {

            XPath xpathEvaluator = xPathfactory.newXPath();
            if(nsContext != null){
                xpathEvaluator.setNamespaceContext(nsContext);
            }
            return xpathEvaluator.evaluate(xpath, xmlItem, dataType);
        } catch (XPathExpressionException e) {
            throw new IllegalArgumentException(String.format("Invallid xpath expression: %s.", xpath), e);
        }
    }

}
package com.atmosquare.commons.reflection.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MethodCatcherHandler implements InvocationHandler {

    private Method method;

    public Method getMethod() {
        return method;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        // android chama implicitamente toString, e o java não
        if(!method.getName().equals("toString")){
            this.method = method;
        }
        return null;
    }
}

package com.atmosquare.commons.reflection;

import java.io.Serializable;
import java.lang.invoke.SerializedLambda;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

import static java.util.Arrays.asList;

public class SerializedLambdaUtils {

    public static <T> String printPropertyName(SerializableSupplier<T> getter) {
        return getter.method().getName();
    }

    public static <T> String getPropertyName(SerializableConsumer<T> methodReference) {
        return getterNameToFieldName(getConsumerName(methodReference));
    }

    private static String getterNameToFieldName(String getterName){

        if(getterName == null){
            return null;
        }

        return capitalize(getterName.replace("get", "").replace("is", ""));
    }

    public static <T, TF> String getFieldName(SerializableGetter<T, TF> methodReference) {
        Method m = methodReference.method();
        if(m != null){
            return getterNameToFieldName(m.getName());
        }
        return null;
    }

    public static String capitalize(String input) {
        return input.substring(0, 1).toLowerCase() + input.substring(1);
    }

    public static <T> String getSupplierName(SerializableSupplier<T> supplier) {
        return supplier.method().getName();
    }

    public static <T> String getConsumerName(SerializableConsumer<T> consumer) {
        return consumer.method().getName();
    }

    // https://gist.github.com/thomasdarimont/034084902c859f46903b1e2aea2790e5
    public interface MethodReferenceReflection {

        //inspired by: http://benjiweber.co.uk/blog/2015/08/17/lambda-parameter-names-with-reflection/

        default SerializedLambda serialized() {
            try {
                Method replaceMethod = getClass().getDeclaredMethod("writeReplace");
                replaceMethod.setAccessible(true);
                return (SerializedLambda) replaceMethod.invoke(this);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        default Class<?> getContainingClass() {
            try {
                String className = serialized().getImplClass().replaceAll("/", ".");
                return Class.forName(className);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        default Method method() {
            SerializedLambda serializedLambda = serialized();
            Class<?> containingClass = getContainingClass();
            return asList(containingClass.getDeclaredMethods())
                    .stream()
                    .filter(method -> Objects.equals(method.getName(), serializedLambda.getImplMethodName())) //TODO check parameter types to deal with overloads
                    .findFirst()
                    .orElseThrow(UnableToGuessMethodException::new);
        }

        class UnableToGuessMethodException extends RuntimeException {
        }
    }

    public interface SerializableSupplier<T> extends Supplier<T>, Serializable, MethodReferenceReflection {
    }

    public interface SerializableConsumer<T> extends Consumer<T>, Serializable, MethodReferenceReflection {
    }

    public interface SerializableGetter<T, TF> extends Function<T, TF>, Serializable, MethodReferenceReflection {
    }

}

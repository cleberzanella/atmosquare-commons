package com.atmosquare.commons.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

// https://github.com/clzanella/nano-frm/blob/master/src/main/java/com/evolutionarylabs/nanofrm/DynamicProxy.java
public class DynamicProxyUtils {

    public static final IProxyFactory JDK_FACTORY = new IProxyFactory(){

        @Override
        public <T> T proxy(Class<T> interf, InvocationHandler handler) {
            return (T) Proxy.newProxyInstance(interf.getClassLoader(), new Class<?>[] { interf }, handler);
        }

    };

    public interface IProxyFactory {

        <T> T proxy(Class<T> classOrInterface, InvocationHandler handler);

    }

    public  static <T> T proxy(Class<T> clazz, InvocationHandler handler, IProxyFactory proxyFactory){
        return proxyFactory.proxy(clazz, handler);
    }

    private DynamicProxyUtils() {}
}
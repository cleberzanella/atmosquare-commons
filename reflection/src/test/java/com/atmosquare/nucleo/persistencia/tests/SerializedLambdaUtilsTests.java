package com.atmosquare.nucleo.persistencia.tests;

import com.atmosquare.commons.reflection.DynamicProxyUtils;
import com.atmosquare.commons.reflection.SerializedLambdaUtils;
import com.atmosquare.commons.reflection.proxy.MethodCatcherHandler;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SerializedLambdaUtilsTests {

    @Test
    void fieldName() {

        MethodCatcherHandler handler = new MethodCatcherHandler();
        IPerson impl = DynamicProxyUtils.proxy(IPerson.class, handler, DynamicProxyUtils.JDK_FACTORY);

        impl.getName();

        assertEquals("getName", handler.getMethod().getName());
    }

    @Test
    void fieldName2() {
        assertEquals("name", SerializedLambdaUtils.getFieldName(Person::getName));

        assertEquals("name", SerializedLambdaUtils.getPropertyName(Person::getName));
        assertEquals("getName", SerializedLambdaUtils.getConsumerName(Person::getName));
        //assertEquals("getName", SerializedLambdaUtils.getConsumerName((Person p) -> p.getName())); // não funciona
        //assertEquals("setName", SerializedLambdaUtils.getSupplierName(Person::setName));
    }


    public interface IPerson {

        String getName();
    }

    public static class Person implements IPerson {

        private String name;

        @Override
        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

}

package com.atmosquare.commons.lang;

import java.util.HashMap;
import java.util.Map;

public class StringUtils {

    public static boolean isNullOrEmpty(String str){
        return str == null || str.isEmpty();
    }

    public static boolean isNullOrWhitespace(String str){

        if(!isNullOrEmpty(str)){
            str = str.trim();
        }

        return isNullOrEmpty(str);
    }

    public static String trimToLength(String text, int trimLength, String suffixIfTrim) {

        String str = text.substring(0, Math.min(text.length(), trimLength));

        if(text.length() > trimLength){
            str += suffixIfTrim;
        }

        return str;
    }

    public static String nullIfEmpty(String strVal) {
        if(isNullOrEmpty(strVal)){
            return null;
        }
        return strVal;
    }

    public static String replaceLast(String string, String toReplace, String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos)
                    + replacement
                    + string.substring(pos + toReplace.length());
        } else {
            return string;
        }
    }

    //args = new String[] { "url=http://1.2.3.4:389", "username=myname@company.fi", "password=mypwd" };
    public static Map<String,String> createParams(String[] args) {
        Map<String,String> params = new HashMap<String,String>();
        for(String str : args) {
            int delim = str.indexOf('=');
            if (delim>0) params.put(str.substring(0, delim).trim(), str.substring(delim+1).trim());
            else if (delim==0) params.put("", str.substring(1).trim());
            else params.put(str, null);
        }
        return params;
    }

    public static String removeNonPrintableCharacters(String input) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            if (Character.isDefined(c) && !Character.isISOControl(c)) {
                result.append(c);
            }
        }

        return result.toString();
    }

    // https://stackoverflow.com/questions/624581/what-is-the-best-java-email-address-validation-method
    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}

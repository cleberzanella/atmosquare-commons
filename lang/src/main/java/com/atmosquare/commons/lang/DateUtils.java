package com.atmosquare.commons.lang;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class DateUtils {

//    public static LocalDate least(LocalDate a, LocalDate b) {
//        return a == null ? b : (b == null ? a : (a.isBefore(b) ? a : b));
//    }

    public static String format(Date date, String format){
        DateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
}

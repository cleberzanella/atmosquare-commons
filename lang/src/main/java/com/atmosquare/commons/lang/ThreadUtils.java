package com.atmosquare.commons.lang;

import java.util.concurrent.TimeUnit;

public class ThreadUtils {

    public static void sleep(long timeOut, TimeUnit timeUnit){
        try {
            timeUnit.sleep(timeOut);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }

}

package com.atmosquare.commons.lang;

import java.util.Locale;

public class LocaleUtils {

    public static final Locale LOCALE_BRAZIL = new Locale("pt", "BR");
    public static final Locale DEFAULT_LOCALE = Locale.US;

}

package com.atmosquare.commons.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.FieldPosition;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class DateTimeProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeProvider.class);

    public static final String SECOND_PRECISION = "yyyy-MM-dd'T'HH:mm:ssZ";
    public static final String MILIS_PRECISION = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

    public Date nowDate(){
        return new Date();
    }

    public int getField(Date date, int field){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        return calendar.get(field);
    }

    public DateFormat iso8601UTCFormatter(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.US);
        df.setTimeZone(TimeZone.getTimeZone("UTC"));

        return df;
    }

    public Date parseISO8601UTC(String dateTimeStr){
        try {
            return iso8601UTCFormatter().parse(dateTimeStr);
        } catch (ParseException e) {
            LOGGER.error(String.format("Error parsing ISO8601 string \"%s\" to date", dateTimeStr), e);
            return null;
        }
    }

    public static DateFormat getIsoOffsetDateTimeFormatter(TimeZone timeZone){
        return getIsoOffsetDateTimeFormatter(SECOND_PRECISION, timeZone);
    }

    public static DateFormat getIsoOffsetDateTimeFormatter(final String pattern, TimeZone timeZone) {

        // compativel com android 16: https://stackoverflow.com/questions/28373610/android-parse-string-to-date-unknown-pattern-character-x
        DateFormat df = new SimpleDateFormat(pattern) {
            @Override
            public Date parse(String source) throws ParseException {
                // o caractere ":" não interfere no parse
                return super.parse(source);
            }

            @Override
            public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition pos) {
                StringBuffer buffer = super.format(date, toAppendTo, pos);
                buffer.insert(pattern.length(), ":");
                return buffer;
            }
        };

        //df.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
        //df.setTimeZone(TimeZone.getTimeZone("GMT-3:00"));
        df.setTimeZone(timeZone);

        return df;
    }

}

package com.atmosquare.commons.lang;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.stream.Collectors;

public class ResourceUtils {

    public static Map<String, String> getResourceFolderFiles(ClassLoader classLoader, String resourceFolderPath){

        URL dirURL = classLoader.getResource(resourceFolderPath);

        if (dirURL.getProtocol().equals("jar")) {

            return getJarResourceFolderFiles(resourceFolderPath, dirURL);
        }

        return getFileResourceFolderFiles(classLoader, resourceFolderPath, dirURL);
    }

    protected static Map<String, String> getFileResourceFolderFiles(ClassLoader classLoader, String resourceFolderPath, URL dirURL) {

        String[] files;
        try {
            files = Paths.get(dirURL.toURI()).toFile().list();
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }

        Map<String, String> entriesMap = new HashMap<>();

        if(files != null){
            for(String fileName : files){

                String resourcePath = Paths.get(resourceFolderPath, fileName).toString();
                URL resourceUrl = classLoader.getResource(resourcePath);
                if(resourceUrl != null){
                    entriesMap.put(fileName, resourcePath);
                }
            }
        }

        return entriesMap;
    }

    public static String getResourceFileAsString(String resourcePath, ClassLoader classLoader) {
        try (InputStream is = classLoader.getResourceAsStream(resourcePath)) {
            if (is == null) {
                return null;
            }
            try (InputStreamReader isr = new InputStreamReader(is);
                 BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().collect(Collectors.joining(System.lineSeparator()));
            }
        } catch (IOException exc){
            throw new IllegalStateException(exc);
        }
    }

    // http://www.uofr.net/~greg/java/get-resource-listing.html
    protected static Map<String, String> getJarResourceFolderFiles(String resourceFolderPath, URL dirURL) {

        String jarPath = dirURL.getPath().substring(5, dirURL.getPath().indexOf("!"));
        JarFile jar;
        try {
            jar = new JarFile(URLDecoder.decode(jarPath, StandardCharsets.UTF_8.name()));
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        Enumeration<JarEntry> entries = jar.entries();

        Map<String, String> entriesMap = new HashMap<>();
        while(entries.hasMoreElements()) {
            JarEntry jarEntry = entries.nextElement();
            if (jarEntry.getName().startsWith(resourceFolderPath)) {
                String entry = jarEntry.getName().substring(resourceFolderPath.length());
                entriesMap.put(entry, jarEntry.getName());
            }
        }

        return entriesMap;
    }
}

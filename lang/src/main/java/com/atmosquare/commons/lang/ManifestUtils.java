package com.atmosquare.commons.lang;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URL;
import java.util.jar.Manifest;

public class ManifestUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManifestUtils.class);

    public static Manifest readManifest(Class<?> clazz){

        String className = clazz.getSimpleName() + ".class";
        String classPath = clazz.getResource(className).toString();
        if (!classPath.startsWith("jar")) {
            // Class is not from JAR
            return null;
        }

        String manifestPath = classPath.substring(0, classPath.lastIndexOf("!") + 1) +
                "/META-INF/MANIFEST.MF";

        try {
            return new Manifest(new URL(manifestPath).openStream());
        } catch (IOException e) {
            LOGGER.error("Error getting Jar manifest attributes", e);
        }

        return null;
    }

    public static String readSpecificationVersion(Class<?> clazz){

        Manifest manifest = readManifest(clazz);

        if(manifest == null){
            return null;
        }

        return manifest.getMainAttributes().getValue("Specification-Version");
    }

    // copiado de org.bytedeco.javacpp.Loader.Detector.getPlatform()
    public static String getPlatform() {
        String jvmName = System.getProperty("java.vm.name", "").toLowerCase();
        String osName  = System.getProperty("os.name", "").toLowerCase();
        String osArch  = System.getProperty("os.arch", "").toLowerCase();
        String abiType = System.getProperty("sun.arch.abi", "").toLowerCase();
        String libPath = System.getProperty("sun.boot.library.path", "").toLowerCase();
        if (jvmName.startsWith("dalvik") && osName.startsWith("linux")) {
            osName = "android";
        } else if (jvmName.startsWith("robovm") && osName.startsWith("darwin")) {
            osName = "ios";
            osArch = "arm";
        } else if (osName.startsWith("mac os x") || osName.startsWith("darwin")) {
            osName = "macosx";
        } else {
            int spaceIndex = osName.indexOf(' ');
            if (spaceIndex > 0) {
                osName = osName.substring(0, spaceIndex);
            }
        }
        if (osArch.equals("i386") || osArch.equals("i486") || osArch.equals("i586") || osArch.equals("i686")) {
            osArch = "x86";
        } else if (osArch.equals("amd64") || osArch.equals("x86-64") || osArch.equals("x64")) {
            osArch = "x86_64";
        } else if (osArch.startsWith("aarch64") || osArch.startsWith("armv8") || osArch.startsWith("arm64")) {
            osArch = "arm64";
        } else if ((osArch.startsWith("arm")) && ((abiType.equals("gnueabihf")) || (libPath.contains("openjdk-armhf")))) {
            osArch = "armhf";
        } else if (osArch.startsWith("arm")) {
            osArch = "arm";
        }
        return System.getProperty("org.bytedeco.javacpp.platform", osName + "-" + osArch);
    }
}

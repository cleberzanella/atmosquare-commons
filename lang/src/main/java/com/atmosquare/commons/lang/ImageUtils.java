package com.atmosquare.commons.lang;

import java.awt.image.BufferedImage;

public class ImageUtils {

    // https://stackoverflow.com/questions/16497853/scale-a-bufferedimage-the-fastest-and-easiest-way
    public static BufferedImage scale(BufferedImage src, int w, int h) {
        BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        int x, y;
        int ww = src.getWidth();
        int hh = src.getHeight();
        for (x = 0; x < w; x++) {
            for (y = 0; y < h; y++) {
                int col = src.getRGB(x * ww / w, y * hh / h);
                img.setRGB(x, y, col);
            }
        }
        return img;
    }

}

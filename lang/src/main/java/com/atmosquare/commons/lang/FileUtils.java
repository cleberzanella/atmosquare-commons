package com.atmosquare.commons.lang;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class FileUtils {

    // https://stackoverflow.com/questions/6561172/find-directory-for-application-data-on-linux-and-macintosh
    // So for linux, either put your folder under $HOME/.config, or start the name with the .
    // Mac pascal case Atmsqr/VideoSurveillance
    // linux .atmsqr/videosurveillance
    // windows, same of mac
    public static String defaultAppDirectory()
    {
        String OS = System.getProperty("os.name").toUpperCase();
        if (OS.contains("WIN")) {

            String dir = System.getenv("APPDATA");

            // rodando como serviço pode retornar: C:\WINDOWS\system32\config\systemprofile
            if(dir != null && dir.contains("system32")){
                return "C:\\Users\\Default\\AppData\\Roaming";
            }

            return dir;
        }
        else if (OS.contains("MAC"))
            return System.getProperty("user.home") + "/Library/Application "
                    + "Support";
        else if (OS.contains("NUX"))
            return "/usr/local/share/";//System.getProperty("user.home");
        return System.getProperty("user.dir");
    }
    public static void copy(InputStream source, File dest, boolean append) {
        InputStream is = null;
        OutputStream os = null;
        try {
            try {
                is = source;
                os = new FileOutputStream(dest, append);
                byte[] buffer = new byte[1024];
                int length;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            } finally {
                is.close();
                os.close();
            }
        } catch (IOException exc){
            throw new RuntimeException(exc);
        }
    }

    public static byte[] readAsBytes(File file){
        InputStream is = null;
        Exception exc = null;
        try {
            return readAsBytes(is = new FileInputStream(file));
        } catch (FileNotFoundException e) {
            exc = e;
            throw new RuntimeException(e);
        } finally {
            if(is != null){
                try {
                    is.close();
                } catch (IOException e) {
                    if(exc == null){
                        throw new RuntimeException(e);
                    } else {
                        exc.addSuppressed(e);
                    }
                }
            }
        }

    }
    public static byte[] readAsBytes(InputStream inputStream){
        final int bufLen = 1024;
        byte[] buf = new byte[bufLen];
        int readLen;
        IOException exception = null;

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            while ((readLen = inputStream.read(buf, 0, bufLen)) != -1)
                outputStream.write(buf, 0, readLen);

            return outputStream.toByteArray();
        } catch (IOException e) {
            exception = e;
            throw new RuntimeException(e);
        } finally {
            if (exception == null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    throw  new RuntimeException(e);
                }
            }
            else try {
                inputStream.close();
            } catch (IOException e) {
                exception.addSuppressed(e);
            }
        }
    }
    public static String readAsString(File file){

        try {

            InputStream is = new FileInputStream(file);
            BufferedReader buf = new BufferedReader(new InputStreamReader(is));

            String line = buf.readLine();
            StringBuilder sb = new StringBuilder();

            while(line != null){
                sb.append(line).append("\n");
                line = buf.readLine();
            }

            return sb.toString();

        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null) {
            for (File file : allContents) {
                deleteDirectory(file);
            }
        }
        return directoryToBeDeleted.delete();
    }

    // https://mkyong.com/java/how-to-decompress-files-from-a-zip-file/
    public static void unzipToFolder(InputStream source, Path target) {

        try (ZipInputStream zis = new ZipInputStream(source)) {

            // list files in zip
            ZipEntry zipEntry = zis.getNextEntry();

            while (zipEntry != null) {

                boolean isDirectory = false;
                // example 1.1
                // some zip stored files and folders separately
                // e.g data/
                //     data/folder/
                //     data/folder/file.txt
                if (zipEntry.getName().endsWith(File.separator)) {
                    isDirectory = true;
                }

                Path newPath = zipSlipProtect(zipEntry, target);

                if (isDirectory) {
                    Files.createDirectories(newPath);
                } else {

                    // example 1.2
                    // some zip stored file path only, need create parent directories
                    // e.g data/folder/file.txt
                    if (newPath.getParent() != null) {
                        if (Files.notExists(newPath.getParent())) {
                            Files.createDirectories(newPath.getParent());
                        }
                    }

                    // copy files, nio
                    //Files.copy(zis, newPath, StandardCopyOption.REPLACE_EXISTING);

                    // copy files, classic
                    try (FileOutputStream fos = new FileOutputStream(newPath.toFile())) {
                        byte[] buffer = new byte[1024];
                        int len;
                        while ((len = zis.read(buffer)) > 0) {
                            fos.write(buffer, 0, len);
                        }
                    }
                }

                zipEntry = zis.getNextEntry();

            }
            zis.closeEntry();

        } catch (IOException exc){
            throw new RuntimeException("Falha ao descompactar zip", exc);
        }
    }

    // protect zip slip attack
    private static Path zipSlipProtect(ZipEntry zipEntry, Path targetDir)
            throws IOException {

        // test zip slip vulnerability
        // Path targetDirResolved = targetDir.resolve("../../" + zipEntry.getName());

        Path targetDirResolved = targetDir.resolve(zipEntry.getName());

        // make sure normalized file still has targetDir as its prefix
        // else throws exception
        Path normalizePath = targetDirResolved.normalize();
        if (!normalizePath.startsWith(targetDir)) {
            throw new IOException("Bad zip entry: " + zipEntry.getName());
        }

        return normalizePath;
    }

    public static File createTempDirectory(String prefix) {
        final File temp;

        try {
            temp = File.createTempFile(prefix == null ? prefix : "temp", Long.toString(System.nanoTime()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        if(!(temp.delete()))
        {
            throw new RuntimeException("Could not delete temp file: " + temp.getAbsolutePath());
        }

        if(!(temp.mkdir()))
        {
            throw new RuntimeException("Could not create temp directory: " + temp.getAbsolutePath());
        }

        return temp;
    }

    public static void writeBytes(File file, byte[] bytes) {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(bytes);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}

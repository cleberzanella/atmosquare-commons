package com.atmosquare.commons.lang;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.util.Locale;

public class NumberUtils {

    // versão NumberUtils.toInt do Apache Commons não é nullable
    public static Integer toIntOrElse(String intValue, Integer defaultValue){
        try {
            return Integer.parseInt(intValue);
        } catch (NumberFormatException exc){
            return defaultValue;
        }
    }

    public static Long toLongOrElse(String longValue, Long defaultValue){
        try {
            return Long.parseLong(longValue);
        } catch (NumberFormatException exc){
            return defaultValue;
        }
    }

    public static BigDecimal toDecimalOrElse(String decimalValue, BigDecimal defaultValue){
        return toDecimalOrElse(decimalValue, defaultValue, LocaleUtils.DEFAULT_LOCALE);
    }

    public static BigDecimal toDecimalOrElse(String decimalValue, BigDecimal defaultValue, Locale locale){
        DecimalFormat format = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(locale));
        return toDecimalOrElse(format, decimalValue, defaultValue);
    }

    public static BigDecimal toDecimalOrElse(DecimalFormat format, String decimalValue, BigDecimal defaultValue){
        try {
            format.setParseBigDecimal(true);
            return (BigDecimal) format.parseObject(decimalValue);
        } catch (ParseException e) {
            return defaultValue;
        }
    }

    public static Double toDoubleOrElse(String decimalValue, Double defaultValue){
        DecimalFormat format = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(LocaleUtils.DEFAULT_LOCALE));
        return toDoubleOrElse(format, decimalValue, defaultValue);
    }

    public static Double toDoubleOrElse(DecimalFormat format, String decimalValue, Double defaultValue){

        BigDecimal number = toDecimalOrElse(format, decimalValue, defaultValue == null ? null : new BigDecimal(defaultValue));

        if(number != null){
            return number.doubleValue();
        }

        return null;
    }

    public static String padLeft(int intNumber, int paddings){
        return padLeft((long) intNumber, paddings);
    }

    public static String padLeft(long longNumber, int paddings){
        return String.format("%0" + paddings + "d", longNumber);
    }

}

package com.atmosquare.commons.sqlite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.FutureTask;

public class DBTask<T> extends FutureTask<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DBTask.class);

    private ExecutorService executorService;

    public DBTask(Callable<T> callable, ExecutorService executorService) {
        super(callable);
        this.executorService = executorService;
    }

    @Override
    public T get() {
        try {
            return super.get();
        } catch (InterruptedException | ExecutionException e) {
            LOGGER.error("DB execution error", e);
            throw new IllegalStateException(e);
        }
    }

    public void onDone(final AsyncDone<T> response){

        executorService.submit(new Runnable() {
            @Override
            public void run() {

                try {
                    response.onResponse(DBTask.this.get());
                }catch (IllegalStateException e){
                    if(e.getCause() instanceof InterruptedException){
                        response.onCancel();
                    } else {
                        response.onException(e);
                    }
                }
            }
        });

    }

    public static abstract class AsyncDone<T> {
        public abstract void onResponse(T value);
        public void onCancel(){
            LOGGER.warn(String.format("AsyncDone cancelled: %s", this.getClass()));
        }
        public void onException(Exception e) {
            LOGGER.error("AsyncDone error", e);
        }
    }
}

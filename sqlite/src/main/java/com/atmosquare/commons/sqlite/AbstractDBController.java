package com.atmosquare.commons.sqlite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Auxilia a criar, atualizar e acessar uma base SQLite.
 */
public abstract class AbstractDBController implements AutoCloseable {

    private static final Logger LOGGER = LoggerFactory.getLogger(NativeSqlExecutorFactory.class);

    private int dbVersion;
    private ISqlExecutor.Factory sqlExecutorFactory;
    private ISqlExecutor sqlExecutor;

    public AbstractDBController(int dbVersion, ISqlExecutor.Factory connectionFactory) {
        this.dbVersion = dbVersion;
        this.sqlExecutorFactory = connectionFactory;
    }

    public ISqlExecutor getSqlExecutor(){
        return sqlExecutor;
    }

    public void initialize() {

        sqlExecutor = sqlExecutorFactory.create();

        if(sqlExecutorFactory.isNewDatabase()){
            LOGGER.info("Criando base SQLite {}", getClass().getSimpleName());
            getSqlExecutor().executeUpdate("PRAGMA user_version = 1;");
            onCreate(getSqlExecutor());
        }

        int actualVersion = ((Number) getSqlExecutor().executeScalar("PRAGMA user_version;")).intValue();
        if(dbVersion > actualVersion){
            LOGGER.info("Atualizando base SQLite {} da versão {} para a versão {}.", getClass().getSimpleName(), actualVersion, dbVersion);
            onUpgrade(getSqlExecutor(), actualVersion, dbVersion);
            getSqlExecutor().executeUpdate("PRAGMA user_version = " + dbVersion + ";");
        }

    }

    public abstract void onCreate(ISqlExecutor db);

    public void onUpgrade(ISqlExecutor db, int oldVersion, int newVersion){

    }

    public synchronized  <T> T sycCall(DBCallable<T> dbCallable){
        return dbCallable.call(getSqlExecutor());
    }

    @Override
    public void close() throws Exception {
        sqlExecutor.close();
    }

    public interface DBCallable<T> {
        T call(ISqlExecutor dbExec);
    }
}

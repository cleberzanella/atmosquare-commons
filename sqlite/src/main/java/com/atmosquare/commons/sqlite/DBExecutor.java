package com.atmosquare.commons.sqlite;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Persistência SQLite é assíncrona para poder ser chamada da UI Thread na plataforma Android.
 * https://github.com/dannysu/sqlitefutures/blob/master/sqlitefutures/src/main/java/com/dannysu/sqlitefutures/SQLiteFutures.java
 */
public class DBExecutor {

    private static DBExecutor[] instances;

    private final AbstractDBController dbController;
    private final ExecutorService responsesExecutorService;

    public static boolean isInitialized(){
        return instances != null;
    }

    public static synchronized void initialize(AbstractDBController... databaseHelper) {
        if (instances == null) {

            instances = new DBExecutor[databaseHelper.length];

            for(int i = 0; i < databaseHelper.length; i++){
                databaseHelper[i].initialize();
                instances[i] = new DBExecutor(databaseHelper[i]);
            }
        }
        else {
            throw new IllegalStateException("Already initialized");
        }
    }

    public static synchronized DBExecutor getInstance(int position) {
        if (instances == null) {
            throw new IllegalStateException("Not initialized");
        }

        return instances[position];
    }

    public static int cleanInstances() {
        int count = instances.length;
        instances = null;
        return count;
    }

    private DBExecutor(AbstractDBController helper) {
        dbController = helper;
        responsesExecutorService = Executors.newCachedThreadPool();
    }

    public <T> DBTask<T> execute(final AbstractDBController.DBCallable<T> callable) {

        DBTask<T> futureTask = new DBTask<>(
                new Callable<T>() {

                    @Override
                    public T call() {
                        return dbController.sycCall(callable);
                    }
                },
                responsesExecutorService);

        // Run on separate background thread
        Thread t = new Thread(futureTask);
        t.start();

        return futureTask;
    }

}

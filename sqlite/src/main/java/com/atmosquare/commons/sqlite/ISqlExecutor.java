package com.atmosquare.commons.sqlite;

import java.util.List;

public interface ISqlExecutor extends AutoCloseable {

    Object executeScalar(String sql);
    int executeUpdate(String sql);
    Object executeUpdateReturningKey(String sql);
    List<Row> executeQuery(String sql);

    interface Factory {
        boolean isNewDatabase();
        ISqlExecutor create();
    }
}

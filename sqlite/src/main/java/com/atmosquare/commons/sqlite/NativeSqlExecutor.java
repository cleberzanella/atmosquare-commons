package com.atmosquare.commons.sqlite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Collections;
import java.util.List;

public class NativeSqlExecutor implements ISqlExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(NativeSqlExecutor.class);

    private final Connection connection;

    public NativeSqlExecutor(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Object executeScalar(String sql) {

        LOGGER.debug(String.format("SQL: %s", sql));
        try(Statement stmt = connection.createStatement()){

            ResultSet resultSet = stmt.executeQuery(sql);

            if(resultSet != null && resultSet.next()){
                return resultSet.getObject(1);
            }

        } catch (SQLException e) {
            LOGGER.error("SQL error", e);
        }

        return null;
    }

    @Override
    public int executeUpdate(String sql) {
        LOGGER.debug(String.format("SQL: %s", sql));
        try (PreparedStatement statement = connection.prepareStatement(sql)) {

            int affectedRows = statement.executeUpdate();
            LOGGER.debug(String.format("SQL affected rows: %s", affectedRows));

            return affectedRows;
        } catch (SQLException e) {
            LOGGER.error("SQL error", e);
        }

        return 0;
    }

    @Override
    public Object executeUpdateReturningKey(String sql) {

        LOGGER.debug(String.format("SQL: %s", sql));
        try (PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {

            int affectedRows = statement.executeUpdate();
            LOGGER.debug(String.format("SQL affected rows: %s", affectedRows));

            if (affectedRows == 0) {
                return null;
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    return generatedKeys.getObject(1);
                }
            }
        } catch (SQLException e) {
            LOGGER.error("SQL error", e);
        }

        return null;
    }

    @Override
    public List<Row> executeQuery(String sql) {
        //LOGGER.debug(String.format("SQL: %s", sql));

        List<Row> rows;
        try(Statement stmt = connection.createStatement()){

            rows = Row.fromResultSet(stmt.executeQuery(sql));
        } catch (SQLException e) {
            LOGGER.error("SQL error", e);
            rows = Collections.emptyList();
        }
        //LOGGER.debug(String.format("SQL returned rows: %s", rows.size()));
        return rows;
    }

    @Override
    public void close() {

        try {
            connection.close();
        } catch (Exception e) {
            LOGGER.error("Error closing SQLite connection.", e);
        }

    }
}

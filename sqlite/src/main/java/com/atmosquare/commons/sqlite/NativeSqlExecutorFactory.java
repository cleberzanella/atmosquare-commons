package com.atmosquare.commons.sqlite;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class NativeSqlExecutorFactory implements ISqlExecutor.Factory {

    private static final Logger LOGGER = LoggerFactory.getLogger(NativeSqlExecutorFactory.class);

    private String dbFileName;
    private File dbFolder;
    private boolean newDatabase;
    private Properties connectionProperties;

    private Connection connection;

    public NativeSqlExecutorFactory(Connection connection, boolean isNewDatabase){
        this.connection = connection;
        this.newDatabase = isNewDatabase;
    }

    public NativeSqlExecutorFactory(File dbFolder, String dbFileName){
        this(dbFolder, dbFileName, new Properties());
    }

    public NativeSqlExecutorFactory(File dbFolder, String dbFileName, Properties connectionProperties) {
        this.dbFileName = dbFileName;
        this.dbFolder = dbFolder;
        this.connectionProperties = connectionProperties;
    }

    @Override
    public boolean isNewDatabase() {
        return newDatabase;
    }

    @Override
    public ISqlExecutor create() {

        if(connection == null){
            File dbFile = new File(dbFolder, dbFileName);

            String connectionString = getConnectionStringFromPath(dbFile);

            if(!dbFolder.exists()){
                dbFolder.mkdirs();
            }

            newDatabase = !dbFile.exists();

            try {
                Connection connection = DriverManager.getConnection(connectionString, connectionProperties);
            } catch (SQLException e){
                throw new IllegalStateException(String.format("Database not started: %s", connectionString), e);
            }
        }

        return new NativeSqlExecutor(connection);
    }

    public static String getConnectionStringFromPath(File dbFile){
        return "jdbc:sqlite:" + dbFile.getAbsolutePath();
    }

}

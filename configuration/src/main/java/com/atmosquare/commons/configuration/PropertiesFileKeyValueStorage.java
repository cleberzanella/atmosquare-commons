package com.atmosquare.commons.configuration;

import com.atmosquare.commons.core.storage.ITransaction;

import java.io.*;
import java.nio.file.Path;
import java.util.Map;
import java.util.Properties;

public class PropertiesFileKeyValueStorage extends AbstractKeyValueStorage {

    private Path propertiesFilePath;
    private Properties config;

    public PropertiesFileKeyValueStorage(Path propertiesFilePath) {
        this.propertiesFilePath = propertiesFilePath;
        loadProperties();
    }

    public Path getPropertiesFilePath() {
        return propertiesFilePath;
    }

    public void loadProperties(){

        File file = propertiesFilePath.toFile();
        try {
            file.createNewFile();
        } catch (IOException exc){
            throw new IllegalArgumentException(exc);
        }

        try {
            config = new Properties();
            config.load(new InputStreamReader(new FileInputStream(file)));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    protected void saveProperties(){
        FileOutputStream writer = null;
        try {
            writer = new FileOutputStream(propertiesFilePath.toFile(), false);
            config.store(writer, null);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
            if(writer != null){
                try {
                    writer.close();
                } catch (IOException e) {
                    throw new IllegalStateException(e);
                }
            }
        }
    }

    @Override
    protected void putValue(String key, String stringValue) {
        config.setProperty(key, stringValue);
    }

    @Override
    protected void removeValue(String key) {
        config.remove(key);
    }

    @Override
    protected String getValue(String key) {
        return config.getProperty(key);
    }

    @Override
    protected ITransaction.TransactionChange createTransactionChange() {
        return new ITransaction.TransactionChange() {
            @Override
            public void onChange(ITransaction transaction) {

                if(transaction.wasCommitted()) {

                    for(Map.Entry<String, String> entry : activeTransaction.getInternalState().entrySet()){
                        if(entry.getValue() != null){
                            putValue(entry.getKey(), entry.getValue());
                        } else {
                            removeValue(entry.getKey());
                        }
                    }
                    saveProperties();

                    activeTransaction = null;
                    return;
                }

                if(transaction.wasRolledBack()){
                    activeTransaction = null;
                }

            }
        };
    }

}

package com.atmosquare.commons.configuration;

import com.atmosquare.commons.core.storage.AbstractTransaction;
import com.atmosquare.commons.core.storage.ICipher;
import com.atmosquare.commons.core.storage.IKeyValueStorage;
import com.atmosquare.commons.core.storage.ITransaction;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractKeyValueStorage implements IKeyValueStorage {

    protected PropertiesFileKeyValueStorage.MemoryTransaction activeTransaction;
    protected ICipher cipher;

    protected abstract void putValue(String key, String stringValue);
    protected abstract void removeValue(String key);
    protected abstract String getValue(String key);

    protected abstract ITransaction.TransactionChange createTransactionChange();

    public void setCipher(ICipher cipher) {
        this.cipher = cipher;
    }

    protected ICipher getCipher() {

        if(cipher == null){
            throw new IllegalStateException("null cipher");
        }

        return cipher;
    }

    @Override
    public ITransaction openTransaction() {

        if(activeTransaction != null && activeTransaction.isActive()){
            throw new IllegalStateException("Close previous transaction before open other");
        }

        activeTransaction = new PropertiesFileKeyValueStorage.MemoryTransaction();
        activeTransaction.setActive(true);
        activeTransaction.setOnChange(createTransactionChange());
        return activeTransaction;
    }

    @Override
    public void putString(String key, String value) {
        checkActiveTransaction();
        activeTransaction.getInternalState().put(key, value);
    }

    @Override
    public void putInt(String key, Integer value) {
        putString(key, value == null ? null : Integer.toString(value));
    }

    @Override
    public void putBoolean(String key, Boolean value) {
        putString(key, value == null ? null : Boolean.toString(value));
    }

    @Override
    public void putEncryptedString(String key, String value){
        String encryptedValue = value == null ? null : getCipher().encrypt(value);
        putString(key, encryptedValue);
    }

    @Override
    public void putEncryptedInt(String key, Integer value){
        putEncryptedString(key, value == null ? null : Integer.toString(value));
    }

    @Override
    public String getString(String key) {

        if(activeTransaction != null && activeTransaction.isActive() && activeTransaction.getInternalState().containsKey(key)){
            return activeTransaction.getInternalState().get(key);
        }

        return  getValue(key);
    }

    @Override
    public Integer getInt(String key) {
        String val = getString(key);
        return val == null || val.isEmpty() ? null : Integer.parseInt(val);
    }

    @Override
    public Integer getIntOrElse(String key, Integer defaultValue){
        Integer value = getInt(key);
        if(value == null){
            return defaultValue;
        }
        return value;
    }

    @Override
    public Boolean getBoolean(String key) {
        String val = getString(key);
        return val == null || val.isEmpty() ? null : Boolean.parseBoolean(val);
    }

    @Override
    public Boolean getBooleanOrElse(String key, Boolean defaultValue) {
        Boolean value = getBoolean(key);
        if(value == null){
            return defaultValue;
        }
        return value;
    }

    @Override
    public String getEncryptedString(String key){
        String val = getString(key);
        if(val == null){
            return  null;
        }
        return getCipher().decrypt(val);
    }

    @Override
    public Integer getEncryptedInt(String key){
        String val = getEncryptedString(key);
        return val == null || val.isEmpty() ? null : Integer.parseInt(val);
    }

    @Override
    public Integer getEncryptedIntOrElse(String key, Integer defaultValue){
        Integer value = getEncryptedInt(key);
        if(value == null){
            return defaultValue;
        }
        return value;
    }

    protected void checkActiveTransaction(){
        if(activeTransaction == null || !activeTransaction.isActive()){
            throw new IllegalStateException("Call openTransaction() before data manipulation");
        }
    }

    protected static class MemoryTransaction extends AbstractTransaction {

        private Map<String, String> internalState = new HashMap<>();

        @Override
        public void setActive(boolean active) {
            super.setActive(active);
        }

        @Override
        public void commit() {

            checkActive();

            setActive(false);
            setCommitted(true);

            if(getOnChangeEvent() != null){
                getOnChangeEvent().onChange(this);
            }

        }

        @Override
        public void rollback() {

            checkActive();

            setActive(false);
            setRolledBack(true);

            if(getOnChangeEvent() != null){
                getOnChangeEvent().onChange(this);
            }

        }

        @Override
        public void close() {
            if(isActive()){
                rollback();
            }
        }

        void checkActive() {
            if(! isActive()){
                throw new IllegalStateException("Transaction must be active");
            }
        }

        public Map<String, String> getInternalState() {
            return internalState;
        }
    }

}

package com.atmosquare.commons.configuration;

import com.atmosquare.commons.core.storage.ITransaction;

import java.util.HashMap;
import java.util.Map;

public class MemoryKeyValueStorage extends AbstractKeyValueStorage {

    private Map<String, String> state = new HashMap<>();

    @Override
    protected void putValue(String key, String stringValue) {
        state.put(key, stringValue);
    }

    @Override
    protected void removeValue(String key) {
        state.remove(key);
    }

    @Override
    protected String getValue(String key) {
        return state.get(key);
    }

    @Override
    protected ITransaction.TransactionChange createTransactionChange() {
        return new ITransaction.TransactionChange() {
            @Override
            public void onChange(ITransaction transaction) {

                if(transaction.wasCommitted()) {

                    for(Map.Entry<String, String> entry : activeTransaction.getInternalState().entrySet()){
                        if(entry.getValue() != null){
                            putValue(entry.getKey(), entry.getValue());
                        } else {
                            removeValue(entry.getKey());
                        }
                    }

                    activeTransaction = null;
                    return;
                }

                if(transaction.wasRolledBack()){
                    activeTransaction = null;
                }

            }
        };
    }

}

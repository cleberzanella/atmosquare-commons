package com.atmosquare.commons.webapi.models;

public class Result<T> {

    private Summary summary;
    private T data;

    private String code;
    private String message;

    public Result(){

    }

    public Result(T data, Summary summary) {
        this.data = data;
        this.summary = summary;
    }

    public Summary getSummary() {
        return summary;
    }

    public void setSummary(Summary summary) {
        this.summary = summary;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

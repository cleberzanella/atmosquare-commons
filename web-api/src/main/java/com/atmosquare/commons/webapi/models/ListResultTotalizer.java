package com.atmosquare.commons.webapi.models;

public class ListResultTotalizer {

    private long totalCount;

    public ListResultTotalizer(){

    }

    public ListResultTotalizer(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }
}

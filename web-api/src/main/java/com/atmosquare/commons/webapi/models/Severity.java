package com.atmosquare.commons.webapi.models;

public enum Severity {
    ERROR,
    WARNING,
    INFO
}

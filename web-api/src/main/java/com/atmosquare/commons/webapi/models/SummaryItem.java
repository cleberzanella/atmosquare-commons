package com.atmosquare.commons.webapi.models;

import java.util.List;

public class SummaryItem {

    private String code;
    private String property;
    private Severity severity = Severity.ERROR;
    private String message;
    private List<String> scopes;

    public SummaryItem(){
    }

    public SummaryItem(String code, String property, Severity severity, String message) {
        this.code = code;
        this.property = property;
        this.severity = severity;
        this.message = message;
    }

    public SummaryItem(Severity severity, String message) {
        this(null, null, severity, message);
    }

    public SummaryItem(String property, String message) {
        this(null, property, Severity.ERROR, message);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }
}

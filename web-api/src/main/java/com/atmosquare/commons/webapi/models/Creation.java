package com.atmosquare.commons.webapi.models;

public class Creation<TID> {

    private TID id;

    public Creation() {
    }

    public Creation(TID id) {
        this.id = id;
    }

    public TID getId() {
        return id;
    }

    public void setId(TID id) {
        this.id = id;
    }
}

package com.atmosquare.commons.webapi.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Summary {

    private List<SummaryItem> items = new ArrayList<>();

    public Summary() {
    }

    public Summary(Summary summary, SummaryItem... items) {

        ArrayList<SummaryItem> lista = new ArrayList<>();
        lista.addAll(summary.getItems());
        lista.addAll(Arrays.asList(items));

        this.items = lista;
    }

    public Summary(SummaryItem... items) {
        this.items = Arrays.asList(items);
    }

    public List<SummaryItem> getItems() {
        return items;
    }

    public void setItems(List<SummaryItem> items) {
        this.items = items;
    }

    public boolean hasErrors(){
        return items.stream().anyMatch(it -> it.getSeverity() == Severity.ERROR);
    }

    public Summary normalize(){

        if(items.isEmpty()){
            return null;
        }

        return this;
    }

}

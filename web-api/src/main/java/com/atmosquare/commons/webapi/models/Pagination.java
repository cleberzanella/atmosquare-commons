package com.atmosquare.commons.webapi.models;

public class Pagination {

    public static final String DEFAULT_PAGE_PARAM = "paging-page";
    public static final String DEFAULT_SIZE_PARAM = "paging-size";
    public static final int DEFAULT_SIZE = 30;
    public static final int MAX_SIZE = 50;

    private int page;
    private int size;

    public Pagination(){

    }

    public Pagination(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public static Pagination normalize(Summary summary, Pagination pagination){
        return normalize(summary, pagination, DEFAULT_SIZE, MAX_SIZE);
    }

    public static Pagination normalize(Summary summary, Pagination pagination, int defaultSize, int maxSize){

        if(pagination == null || (pagination.getPage() == 0 && pagination.getSize() == 0)){
            // página padrão
            return new Pagination(0, defaultSize);
        }

        if(pagination.getSize() == 0){
            pagination.setSize(defaultSize);
        }

        if(pagination.getSize() > maxSize){

            if(summary != null){
                summary.getItems().add(new SummaryItem(
                        Severity.WARNING,
                        String.format("Tamanho da página foi reduzido de %1$s para %2$s, pois o size máximo da página é %2$s.", pagination.getSize(), maxSize)
                ));
            }

            pagination.setSize(maxSize);
        }

        return pagination;
    }

}

package com.atmosquare.commons.webapi.exceptions;

public class ApiResponseException extends IllegalStateException {

    private int httpCode;
    private Object responseObject;

    public ApiResponseException(int httpCode, Object responseObject) {
        super();
        this.httpCode = httpCode;
        this.responseObject = responseObject;
    }

    public int getHttpCode() {
        return httpCode;
    }

    public Object getResponseObject() {
        return responseObject;
    }
}
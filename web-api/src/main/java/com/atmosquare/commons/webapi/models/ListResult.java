package com.atmosquare.commons.webapi.models;

import java.util.List;

public class ListResult<T> extends Result<List<T>> {

    private Pagination pagination;
    private ListResultTotalizer totalizer;

    public ListResult(){

    }

    public ListResult(Pagination pagination, List<T> dados){
        setData(dados);
        this.pagination = pagination;
    }

    public ListResult(Pagination pagination, List<T> dados, ListResultTotalizer totalizer){
        this(pagination, dados);
        this.totalizer = totalizer;
    }

    public ListResult(Pagination pagination, List<T> dados, Summary summary){
        this(pagination, dados);
        setSummary(summary);
    }

    public ListResult(Pagination pagination, List<T> dados, ListResultTotalizer totalizer, Summary summary){
        this(pagination, dados);
        this.totalizer = totalizer;
        setSummary(summary);
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public ListResultTotalizer getTotalizer() {
        return totalizer;
    }

    public void setTotalizer(ListResultTotalizer totalizer) {
        this.totalizer = totalizer;
    }
}

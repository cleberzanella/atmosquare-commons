package com.atmosquare.commons.validation.tests;

import br.com.fluentvalidator.AbstractValidator;
import br.com.fluentvalidator.Validator;
import br.com.fluentvalidator.context.Error;
import br.com.fluentvalidator.context.ValidationResult;
import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;

import static br.com.fluentvalidator.predicate.LogicalPredicate.isFalse;
import static br.com.fluentvalidator.predicate.LogicalPredicate.isTrue;
import static br.com.fluentvalidator.predicate.LogicalPredicate.not;
import static br.com.fluentvalidator.predicate.ObjectPredicate.nullValue;
import static br.com.fluentvalidator.predicate.StringPredicate.*;
import static org.junit.jupiter.api.Assertions.*;

public class ValidationUtilsTests {

    @Test
    void valid(){

        Person person1 = new Person();
        person1.setName("Cleber");
        person1.setAge(18);

        Validator<Person> validator = new PersonValidator();
        ValidationResult validationResult1 = validator.validate(person1);

        assertTrue(validationResult1.isValid());

        Person person2 = new Person();

        ValidationResult validationResult2 = validator.validate(person2);

        assertFalse(validationResult2.isValid());
        assertEquals(2, validationResult2.getErrors().size());

        Error error1 = validationResult2.getErrors().stream().findFirst().get();
        assertEquals("555", error1.getCode());

        Error error2 = validationResult2.getErrors().stream().collect(Collectors.toList()).get(1);
        assertEquals("556", error2.getCode());

        Person person3 = new Person();
        person3.setName("Zé");

        ValidationResult validationResult3 = validator.validate(person3);

        assertFalse(validationResult3.isValid());
        assertEquals(1, validationResult3.getErrors().size());

        error1 = validationResult3.getErrors().stream().findFirst().get();
        assertEquals("556", error1.getCode());

    }

    @Test
    void invalid1() {

    }

    public static class Person {

        private String name;
        private int age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }
    }

    public static class PersonValidator extends AbstractValidator<Person> {

        @Override
        public void rules() {


            ruleFor(Person::getName)
                    .must(not(stringEmptyOrNull()))
                    .withMessage("person's name cannot be null or empty")
                    .withCode("555")
                    .withFieldName("name")
                    .critical();

            ruleFor(Person::getName)
                    .must(stringSizeBetween(4, 50))
                    .withMessage("person's name size must be between 4 and 50 characters")
                    .withCode("556")
                    .withFieldName("name");

        }
    }

}

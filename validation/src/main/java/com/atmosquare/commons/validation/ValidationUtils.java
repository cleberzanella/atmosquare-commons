package com.atmosquare.commons.validation;

import br.com.fluentvalidator.context.ValidationContext;
import br.com.fluentvalidator.context.ValidationResult;

import javax.validation.ConstraintViolation;
import java.util.HashSet;
import java.util.Set;

// https://github.com/mvallim/java-fluent-validator/blob/master/documentation/3-spring-support.md
// javax.validation https://www.baeldung.com/javax-validation
public class ValidationUtils {

    public static <T> Set<ConstraintViolation<T>> validate(T entity, ValidationContext validationContext){

        return null;
    }

    protected static <T> Set<ConstraintViolation<T>> convertResultFromFluent(ValidationResult validationResult){

        Set<ConstraintViolation<T>> set = new HashSet<>();

        ConstraintViolation<T> con = null;
        return set;
    }

    public static class ValidationContext {

    }

    public static class Pessoa {

        private String nome;
        private Integer age;

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }

    public static class PessoaValidador extends AbstractValidator<Pessoa>{

        public PessoaValidador(){

            ruleForString(Pessoa::getNome).notNullOrEmpty();
            ruleFor(Pessoa::getAge).notNull();

        }

    }

    public static void main(String... args){

        Pessoa p1 = new Pessoa();
        p1.setNome("Cleber");
        p1.setAge(31);

        Pessoa p2 = new Pessoa();
        //p2.setNome("Cleber");
        p2.setAge(31);

        Pessoa p3 = new Pessoa();
        p3.setNome("Cleber");
        //p3.setAge(31);


        IValidator<Pessoa> validador = new PessoaValidador();

        validador.validate(p1).hasErrors();
        validador.validate(p2).hasErrors();
        validador.validate(p3).hasErrors();

    }

}

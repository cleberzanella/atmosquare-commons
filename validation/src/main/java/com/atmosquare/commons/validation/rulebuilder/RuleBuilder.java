package com.atmosquare.commons.validation.rulebuilder;

import com.atmosquare.commons.validation.AbstractValidator;
import com.atmosquare.commons.validation.constraints.NotNullConstraint;

public class RuleBuilder<TF> {

    protected AbstractValidator.ContextConstraintController constraintController;

    public RuleBuilder(AbstractValidator.ContextConstraintController constraintController) {
        this.constraintController = constraintController;
    }

    public RuleBuilder<TF> notNull() {
        constraintController.addConstraint(new NotNullConstraint(constraintController.getFieldGetter()));
        return this;
    }
}

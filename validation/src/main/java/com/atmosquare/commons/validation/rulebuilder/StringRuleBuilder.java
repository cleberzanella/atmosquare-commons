package com.atmosquare.commons.validation.rulebuilder;

import com.atmosquare.commons.validation.AbstractValidator;
import com.atmosquare.commons.validation.constraints.StringNotNullOrEmptyConstraint;

public class StringRuleBuilder extends RuleBuilder<String> {

    public StringRuleBuilder(AbstractValidator.ContextConstraintController constraintController) {
        super(constraintController);
    }

    public StringRuleBuilder notNullOrEmpty() {
        constraintController.addConstraint(new StringNotNullOrEmptyConstraint(constraintController.getFieldGetter()));
        return this;
    }

}

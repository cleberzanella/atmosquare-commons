package com.atmosquare.commons.validation.constraints;

import com.atmosquare.commons.validation.Severity;

import java.util.function.Function;

public abstract class AbstractConstraint {

    private FieldGetter fieldGetter;
    private Severity severity = Severity.ERROR;

    public AbstractConstraint(FieldGetter fieldGetter) {
        this.fieldGetter = fieldGetter;
    }

    protected void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public Severity getSeverity() {
        return this.severity;
    }

    public FieldGetter getFieldGetter() {
        return fieldGetter;
    }

    public abstract <T> boolean check(T candidade);

    public static class FieldGetter{

        private String fieldName;
        private Function<Object, Object> getter;

        public FieldGetter(String fieldName, Function<?, ?> getter) {
            this.fieldName = fieldName;
            this.getter = (Function<Object, Object>) getter;
        }

        public String getFieldName() {
            return fieldName;
        }

        public Object getValue(Object rootObject) {
            return getter.apply(rootObject);
        }
    }

}

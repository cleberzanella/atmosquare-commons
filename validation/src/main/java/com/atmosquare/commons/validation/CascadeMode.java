package com.atmosquare.commons.validation;

public enum CascadeMode {
    CONTINUE,
    STOP_ON_FIRST_FAILURE
}

package com.atmosquare.commons.validation.results;

import com.atmosquare.commons.validation.Severity;
import com.atmosquare.commons.validation.constraints.AbstractConstraint;

public class ValidationFailure {

    private String code;
    private String message;
    private String fieldName;
    private Object attemptedValue;
    private Object customState;
    private Severity severity = Severity.ERROR;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public Object getAttemptedValue() {
        return attemptedValue;
    }

    public void setAttemptedValue(Object attemptedValue) {
        this.attemptedValue = attemptedValue;
    }

    public Object getCustomState() {
        return customState;
    }

    public void setCustomState(Object customState) {
        this.customState = customState;
    }

    public Severity getSeverity() {
        return severity;
    }

    public void setSeverity(Severity severity) {
        this.severity = severity;
    }

    public static ValidationFailure fromConstraint(AbstractConstraint constraint) {

        ValidationFailure failure = new ValidationFailure();

        failure.setSeverity(constraint.getSeverity());
        failure.setCode(constraint.getClass().getSimpleName());
        failure.setFieldName(constraint.getFieldGetter().getFieldName());

        return failure;
    }

}

package com.atmosquare.commons.validation.constraints;

public class NotNullConstraint extends AbstractConstraint {

    public NotNullConstraint(FieldGetter fieldGetter) {
        super(fieldGetter);
    }

    @Override
    public <T> boolean check(T candidade) {
        return getFieldGetter().getValue(candidade) != null;
    }
}

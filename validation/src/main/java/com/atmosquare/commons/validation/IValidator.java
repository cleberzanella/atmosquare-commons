package com.atmosquare.commons.validation;

import com.atmosquare.commons.validation.results.ValidationResult;

public interface IValidator<T> {

    void setCascadeMode(CascadeMode cascadeMode);
    CascadeMode getCascadeMode();

    ValidationResult validate(T candidate);

}

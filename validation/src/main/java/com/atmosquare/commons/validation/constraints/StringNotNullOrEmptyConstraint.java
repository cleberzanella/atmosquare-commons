package com.atmosquare.commons.validation.constraints;

public class StringNotNullOrEmptyConstraint extends AbstractConstraint {

    public StringNotNullOrEmptyConstraint(FieldGetter fieldGetter) {
        super(fieldGetter);
    }

    @Override
    public <T> boolean check(T candidade) {

        String strValue = (String) getFieldGetter().getValue(candidade);

        return strValue != null && !strValue.isEmpty();
    }
}

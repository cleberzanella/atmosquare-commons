package com.atmosquare.commons.validation.results;

import com.atmosquare.commons.validation.Severity;

import java.util.List;

public class ValidationResult {

    private List<ValidationFailure> failures;

    public ValidationResult(List<ValidationFailure> failures) {
        this.failures = failures;
    }

    public boolean hasErrors(){
        return failures.stream().anyMatch(f -> f.getSeverity() == Severity.ERROR);
    }

    public List<ValidationFailure> getFailures() {
        return failures;
    }
}

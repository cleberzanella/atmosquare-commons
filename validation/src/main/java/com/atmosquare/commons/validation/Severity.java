package com.atmosquare.commons.validation;

public enum Severity {
    ERROR,
    WARNING,
    INFO
}

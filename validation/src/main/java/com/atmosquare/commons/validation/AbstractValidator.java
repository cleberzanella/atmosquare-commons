package com.atmosquare.commons.validation;

import com.atmosquare.commons.reflection.SerializedLambdaUtils;
import com.atmosquare.commons.validation.constraints.AbstractConstraint;
import com.atmosquare.commons.validation.results.ValidationFailure;
import com.atmosquare.commons.validation.results.ValidationResult;
import com.atmosquare.commons.validation.rulebuilder.RuleBuilder;
import com.atmosquare.commons.validation.rulebuilder.StringRuleBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public abstract class AbstractValidator<T> implements IValidator<T> {

    private CascadeMode cascadeMode = CascadeMode.CONTINUE;
    private List<AbstractConstraint> constraints = new ArrayList<>();


    @Override
    public void setCascadeMode(CascadeMode cascadeMode) {
        this.cascadeMode = cascadeMode;
    }

    @Override
    public CascadeMode getCascadeMode() {
        return this.cascadeMode;
    }

    @Override
    public ValidationResult validate(T candidade) {

        List<ValidationFailure> failures = new ArrayList<>();

        for(AbstractConstraint constraint : constraints){

            boolean success = constraint.check(candidade);
            if(!success){

                ValidationFailure failure = ValidationFailure.fromConstraint(constraint);
                failures.add(failure);

                if(constraint.getSeverity() == Severity.ERROR && this.cascadeMode == CascadeMode.STOP_ON_FIRST_FAILURE){
                    break;
                }

            }

        }

        return new ValidationResult(failures);
    }

    protected <TF> RuleBuilder<TF> ruleFor(SerializedLambdaUtils.SerializableGetter<T, TF> methodReference){

        Function<T, TF> getter = methodReference;
        String fieldName = SerializedLambdaUtils.getFieldName(methodReference);

        ContextConstraintController controller = new ContextConstraintController(constraints, new AbstractConstraint.FieldGetter(fieldName, getter));
        return new RuleBuilder<>(controller);
    }

    protected StringRuleBuilder ruleForString(SerializedLambdaUtils.SerializableGetter<T, String> methodReference){

        Function<T, String> getter = methodReference;
        String fieldName = SerializedLambdaUtils.getFieldName(methodReference);

        ContextConstraintController controller = new ContextConstraintController(constraints, new AbstractConstraint.FieldGetter(fieldName, getter));
        return new StringRuleBuilder(controller);
    }


    public static class ContextConstraintController {

        private List<AbstractConstraint> contextContraints;
        private AbstractConstraint.FieldGetter fieldGetter;

        public ContextConstraintController(List<AbstractConstraint> contextContraints, AbstractConstraint.FieldGetter fieldGetter) {
            this.contextContraints = contextContraints;
            this.fieldGetter = fieldGetter;
        }

        public void addConstraint(AbstractConstraint constraint){
            this.contextContraints.add(constraint);
        }

        public AbstractConstraint.FieldGetter getFieldGetter() {
            return fieldGetter;
        }
    }

}

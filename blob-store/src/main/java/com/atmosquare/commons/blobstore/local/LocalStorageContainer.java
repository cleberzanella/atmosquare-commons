package com.atmosquare.commons.blobstore.local;

import com.atmosquare.commons.blobstore.IBlobReference;
import com.atmosquare.commons.blobstore.IBlobStorageContainer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class LocalStorageContainer implements IBlobStorageContainer {

    private final String localDir;
    private String storageDir;

    public LocalStorageContainer(String localDir){
        this.localDir = localDir;
    }

    private void checkDirExists() {
        String tenantDir;
        if (getTenantId() != null)
        {
            tenantDir = "tenant-" + getTenantId();
        }
        else
        {
            tenantDir = "tenant-default";
        }

        File storageDirFile = new File(new File(localDir), tenantDir);
        storageDir = storageDirFile.getAbsolutePath();

        if (!storageDirFile.exists())
        {
            storageDirFile.mkdirs();
        }
    }

    @Override
    public IBlobReference getBlob(String blobName) {
        checkDirExists();
        return new LocalFileReference(new File(new File(storageDir), blobName).getAbsolutePath());
    }

    @Override
    public List<IBlobReference> listBlobNamesByPrefix(String prefix) {

        class PrefixFilenameFilter implements FilenameFilter {

            String initials;

            public PrefixFilenameFilter(String initials)
            {
                this.initials = initials;
            }

            public boolean accept(File dir, String name)
            {
                return name.startsWith(initials);
            }
        }

        String[] files = new File(storageDir).list(new PrefixFilenameFilter(prefix));
        List<IBlobReference> references = new ArrayList<>(files.length);
        for(String file : files){
            references.add(new LocalFileReference(file.replaceAll(storageDir, "")));
        }

        return references;
    }

    static class LocalFileReference implements IBlobReference {
        private final String localPath;

        public LocalFileReference(String localPath) {
            this.localPath = localPath;
        }


        @Override
        public boolean exists() {
            return new File(localPath).exists();
        }

        @Override
        public String getSimpleName() {
            return new File(localPath).getName();
        }

        @Override
        public void uploadContent(byte[] content, Map<String, String> tags) {
            try (FileOutputStream fos = new FileOutputStream(localPath)) {
                fos.write(content);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public void uploadContent(InputStream inStream, Map<String, String> tags) {
            byte[] buffer = new byte[8 * 1024];
            int bytesRead;
            try (BufferedOutputStream outStream = new BufferedOutputStream(Files.newOutputStream(new File(localPath).toPath()))) {
                while ((bytesRead = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    inStream.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

        @Override
        public byte[] downloadContent() {
            File file = new File(localPath);
            byte[] byteArray = new byte[(int) file.length()];
            try (FileInputStream inputStream = new FileInputStream(file)) {
                inputStream.read(byteArray);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return byteArray;
        }

        @Override
        public OutputStream downloadContentStream() {
            try {
                return new FileOutputStream(localPath);
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        @Override
        public String createPublicUrl(Duration expiration) {
            return null;
        }

        @Override
        public String createPublicUploadUrl(Duration expiration) {
            return null;
        }

        @Override
        public boolean deleteContent() {
            return new File(localPath).delete();
        }

        @Override
        public Map<String, String> getTags() {
            return Collections.emptyMap();
        }
    }
}

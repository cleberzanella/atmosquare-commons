package com.atmosquare.commons.blobstore;

import java.util.List;

public interface IBlobStorageContainer {
    IBlobReference getBlob(String blobName);
    List<IBlobReference> listBlobNamesByPrefix(String prefix);
    String getTenantId();
}

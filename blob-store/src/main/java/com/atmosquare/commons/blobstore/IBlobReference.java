package com.atmosquare.commons.blobstore;

import java.io.InputStream;
import java.io.OutputStream;
import java.time.Duration;
import java.util.Map;

public interface IBlobReference {
    boolean exists();
    String getSimpleName();
    void uploadContent(byte[] content, Map<String, String> tags);
    void uploadContent(InputStream inStream, Map<String, String> tags);
    byte[] downloadContent();
    OutputStream downloadContentStream();
    String createPublicUrl(Duration expiration);

    String createPublicUploadUrl(Duration expiration);

    boolean deleteContent();
    Map<String, String> getTags();
}

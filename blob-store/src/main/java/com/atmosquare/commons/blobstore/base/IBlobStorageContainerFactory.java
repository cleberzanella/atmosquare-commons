package com.atmosquare.commons.blobstore.base;

import com.atmosquare.commons.blobstore.IBlobStorageContainer;

public interface IBlobStorageContainerFactory {
    String getContainerName();
    IBlobStorageContainer create();
}

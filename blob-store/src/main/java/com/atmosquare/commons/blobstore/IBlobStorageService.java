package com.atmosquare.commons.blobstore;

public interface IBlobStorageService {
    IBlobStorageContainer getContainer(String containerName);
}

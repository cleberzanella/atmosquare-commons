package com.atmosquare.commons.blobstore.base;

import com.atmosquare.commons.blobstore.IBlobStorageContainer;
import com.atmosquare.commons.blobstore.IBlobStorageService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class BlobStorageService implements IBlobStorageService {

    protected List<IBlobStorageContainerFactory> factories;

    public BlobStorageService(List<IBlobStorageContainerFactory> factories){
        this.factories = factories;
    }

    @Override
    public IBlobStorageContainer getContainer(String containerName) {
        List<IBlobStorageContainerFactory> containerFacts = factories.stream().filter(fact -> fact.getContainerName().equals(containerName)).collect(Collectors.toList());
        if(containerFacts.size() > 1)
        {
            String factsDesc = containerFacts.stream().map(f -> f.getClass().getSimpleName()).collect(Collectors.joining(","));
            throw new IllegalArgumentException(String.format("Foi requisitado um BlobStorageContainer com o nome %s, mas este nome está sendo usado em mais de uma factory: %s", containerName, factsDesc));
        }

        Optional<IBlobStorageContainerFactory> factoryOpt = containerFacts.stream().findFirst();
        if(!factoryOpt.isPresent())
        {
            String factoriesConfigurados = factories.stream().map(fct -> fct.getContainerName()).collect(Collectors.joining(","));
            throw new IllegalArgumentException(String.format("Foi requisitado um BlobStorageContainer com o nome %s, mas este não está configurado. Configurados: %s", containerName, factoriesConfigurados));
        }

        IBlobStorageContainer container = factoryOpt.get().create();

        onContainerCreated(container);

        return container;
    }

    protected void onContainerCreated(IBlobStorageContainer container){}

}

package com.atmosquare.commons.auth.service;

import com.atmosquare.commons.auth.model.ApiAuthInfo;
import com.atmosquare.commons.auth.model.UserAuthInfo;
import com.atmosquare.commons.json.JsonUtils;
import com.atmosquare.commons.webrequest.WebRequestConfiguration;
import com.atmosquare.commons.webrequest.WebRequestUtils;

import java.util.List;
import java.util.Map;

public class AuthenticationService {

    public static final String AUTH_SERVER_URL_CONFIG = "atmsqr.servidor.autenticacao";
    public static final String DEFAULT_ENDPOINT = "/api/v1/auth";
    public static final String DEFAULT_LOGIN_ENDPOINT = "/users/login";
    public static final String DEFAULT_API_LOGIN_ENDPOINT = "/api-keys/login";

    private String basePath;

    public AuthenticationService(String basePath) {
        this.basePath = basePath;
    }

    public UserAuthInfo loginUserName(String userName, String userPassword, int moduleNumber){

        String apiUrl = basePath + DEFAULT_ENDPOINT + DEFAULT_LOGIN_ENDPOINT;

        String body = "{\"userName\":\"" + userName + "\", \"password\": \"" + userPassword + "\", \"moduleNumber\": " + moduleNumber + "}";

        WebRequestConfiguration conf = new WebRequestConfiguration();
        conf.setHeader("Content-Type", "application/json");

        WebRequestUtils.WebResult ret = WebRequestUtils.request(apiUrl, "POST", body, true, conf);

        if(ret.getHttpCode() != 200){
            return null;
        }

        Map<String, Object> obj1 = JsonUtils.parseMap(ret.getResponseBody());
        Map<String, Object> ob2 = (Map<String, Object>) obj1.get("data");

        UserAuthInfo userInfo = new UserAuthInfo();
        userInfo.setId(((Number) ob2.get("id")).longValue());
        userInfo.setTenantId(((Number) ob2.get("tenantId")).longValue());
        userInfo.setUserName((String) ob2.get("userName"));
        if(ob2.containsKey("jwtToken")){
            userInfo.setJwtToken((String) ob2.get("jwtToken"));
        }
        if(ob2.containsKey("roles")){
            userInfo.setRoles((List<String>) ob2.get("roles"));
        }

        return userInfo;
    }

    public ApiAuthInfo loginApi(String apiKey, int moduleNumber){

        String apiUrl = basePath + DEFAULT_ENDPOINT + DEFAULT_API_LOGIN_ENDPOINT;

        String body = "{\"apiKey\":\"" + apiKey + "\", \"moduleNumber\": " + moduleNumber + "}";

        WebRequestConfiguration conf = new WebRequestConfiguration();
        conf.setHeader("Content-Type", "application/json");

        WebRequestUtils.WebResult ret = WebRequestUtils.request(apiUrl, "POST", body, true, conf);

        if(ret.getHttpCode() != 200){
            return null;
        }

        Map<String, Object> obj1 = JsonUtils.parseMap(ret.getResponseBody());
        Map<String, Object> ob2 = (Map<String, Object>) obj1.get("data");

        ApiAuthInfo authInfo = new ApiAuthInfo();
        authInfo.setId(((Number) ob2.get("id")).longValue());
        authInfo.setTenantId(((Number) ob2.get("tenantId")).longValue());
        authInfo.setApiKeyName((String) ob2.get("apiKeyName"));

        return authInfo;
    }

    public UserAuthInfo loginJwt(String jwtToken){
        return null;
    }


}

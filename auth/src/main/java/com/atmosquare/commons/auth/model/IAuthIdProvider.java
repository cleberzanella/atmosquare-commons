package com.atmosquare.commons.auth.model;

import java.util.List;

public interface IAuthIdProvider {
    Long getAuthId();
    Long getTenantId();
    List<String> getRoles();

    default boolean containsRole(String role){
       if(getRoles() == null){
           return false;
       }
       return getRoles().contains(role);
    }
}

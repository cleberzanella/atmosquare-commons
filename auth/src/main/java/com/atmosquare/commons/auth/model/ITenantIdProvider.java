package com.atmosquare.commons.auth.model;

public interface ITenantIdProvider {
    Long getTenantId();
}

package com.atmosquare.commons.auth.model;

public interface IJwtProvider {
    String getJwt();
}
